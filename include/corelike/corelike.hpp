#ifndef CORELIKE_CORELIKE_HPP
#define CORELIKE_CORELIKE_HPP
//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include "assert.hpp"
#include "content_span.hpp"
#include "narrow_cast.hpp"
#include "span.hpp"
#include "span_algorithm.hpp"
#include "span_allocator.hpp"
#include "string_span.hpp"
#include "thread.hpp"

namespace galik {
namespace corelike {

template<typename Ptr, class = std::enable_if_t<std::is_pointer<Ptr>::value>>
class assert_not_null
{
public:
	using value_type = typename std::remove_reference<decltype(*Ptr())>::type;
	using const_value_type = typename std::remove_const<value_type>::type const;
	using pointer = value_type*;
	using const_pointer = const_value_type*;
	using reference = value_type&;
	using const_reference = const_value_type&;

	assert_not_null(std::nullptr_t) = delete;
	assert_not_null(pointer ptr): ptr(ptr) { CORELIKE_ASSERT(ptr); }

	pointer       get()       { return ptr; }
	const_pointer get() const { return ptr; }

	operator Ptr() const { return ptr; }

	reference       operator*()       { return *ptr; }
	const_reference operator*() const { return *ptr; }

	pointer       operator->()       { return ptr; }
	const_pointer operator->() const { return ptr; }

private:
	pointer ptr;
};

//template<typename Ptr, class = std::enable_if_t<std::is_pointer<Ptr>::value>>
//class null_checked
//{
//public:
//	using value_type = typename std::remove_reference<typename std::remove_const<decltype(*Ptr())>::type>::type;
//	using pointer = Ptr;
//	using const_pointer = value_type const*;
//	using non_const_pointer = value_type*;
//	using reference = decltype(*Ptr());
//	using const_reference = value_type const&;
//	using non_const_reference = value_type&;
//
//	null_checked(std::nullptr_t) = delete;
//	null_checked(pointer ptr): ptr(ptr) { CORELIKE_ASSERT(ptr); }
//
//	pointer       get()       { return ptr; }
//	const_pointer get() const { return ptr; }
//
//	operator Ptr() const { return ptr; }
//
//	reference       operator*()       { return *ptr; }
//	const_reference operator*() const { return *ptr; }
//
//	pointer       operator->()       { return ptr; }
//	const_pointer operator->() const { return ptr; }
//
//private:
//	pointer ptr;
//};

} // namespace corelike
} // namespace galik

#endif // CORELIKE_CORELIKE_HPP
