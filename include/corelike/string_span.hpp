#ifndef CORELIKE_STRING_SPAN_HPP
#define CORELIKE_STRING_SPAN_HPP
//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <algorithm>
#include <ostream> // operator<<()
#include <string>  // operator<<()

#include "span.hpp"

namespace galik {
namespace corelike {

/*! \struct std_int_type_test
	\brief SFNAE test for integer types
 */
template<typename CharT>
struct is_std_char
{
	/*! true or false */
	constexpr static bool value =
	   std::is_same<CharT, char>::value
	|| std::is_same<CharT, char const>::value
	|| std::is_same<CharT, byte>::value
	|| std::is_same<CharT, byte const>::value
	|| std::is_same<CharT, wchar_t>::value
	|| std::is_same<CharT, wchar_t const>::value
	|| std::is_same<CharT, char16_t>::value
	|| std::is_same<CharT, char16_t const>::value
	|| std::is_same<CharT, char32_t>::value
	|| std::is_same<CharT, char32_t const>::value;
};

template<typename CharT, class = typename std::enable_if<is_std_char<CharT>::value>::type>
using basic_string_span = span<CharT>;

using string_span = basic_string_span<char>;
using const_string_span = basic_string_span<char const>;

using wstring_span = basic_string_span<wchar_t>;
using const_wstring_span = basic_string_span<wchar_t const>;

using u16string_span = basic_string_span<char16_t>;
using const_u16string_span = basic_string_span<char16_t const>;

using u32string_span = basic_string_span<char32_t>;
using const_u32string_span = basic_string_span<char32_t const>;

template<typename CharT>
basic_string_span<CharT> make_string_span(CharT* s1, CharT* s2)
	{ return basic_string_span<CharT>(s1, s2); }

template<typename CharT, std::size_t N>
basic_string_span<CharT> make_string_span(CharT(&s)[N])
{
	CORELIKE_ASSERT(s[N - 1] == CharT('\0'));
	return make_string_span<CharT>(s, s + N - 1);
}

template<typename CharT, std::size_t N>
basic_string_span<CharT> make_string_span(std::array<CharT, N>& s)
	{ return make_string_span<CharT>(s.data(), s.data() + N); }

template<typename CharT, std::size_t N>
basic_string_span<const CharT> make_string_span(std::array<CharT, N> const& s)
	{ return make_string_span<CharT>(s, s + N); }

template<typename CharT>
basic_string_span<CharT> make_string_span(CharT* s, index n)
	{ return make_string_span<CharT>(s, s + n); }

//namespace detail {
//
//template<typename CharT>
//std::size_t string_length(CharT* s)
//{
//	return std::size_t(std::find(s, s + PTRDIFF_MAX, CharT('\0')) - s);
//}
//
//} // namespace detail
//
//template<typename CharT>
//basic_string_span<CharT> make_string_span(CharT* s)
//	{ return make_string_span<CharT>(s, s + detail::string_length(s)); }

template<typename CharT, typename Traits, typename Alloc>
basic_string_span<CharT> make_string_span(std::basic_string<CharT, Traits, Alloc>& s)
	{ return make_string_span<CharT>(&s[0], &s[0] + s.size()); }

template<typename CharT, typename Traits, typename Alloc>
auto make_string_span(std::basic_string<CharT, Traits, Alloc> const& s)
	{ return make_string_span<CharT const>(&s[0], &s[0] + s.size()); }

/// FIXED

template<typename CharT, size_type N, class = typename std::enable_if
	<N != dynamic_extent && is_std_char<CharT>::value>::type>
using fixed_basic_string_span = span<CharT, N>;

template<size_type N>
using fixed_string_span = fixed_basic_string_span<char, N>;
template<size_type N>
using const_fixed_string_span = fixed_basic_string_span<char const, N>;

template<size_type N>
using fixed_wstring_span = fixed_basic_string_span<wchar_t, N>;
template<size_type N>
using const_fixed_wstring_span = fixed_basic_string_span<wchar_t const, N>;

template<size_type N>
using fixed_u16string_span = fixed_basic_string_span<char16_t, N>;
template<size_type N>
using const_fixed_u16string_span = fixed_basic_string_span<char16_t const, N>;

template<size_type N>
using fixed_u32string_span = fixed_basic_string_span<char32_t, N>;
template<size_type N>
using const_fixed_u32string_span = fixed_basic_string_span<char32_t const, N>;

template<typename CharT, size_type N>
auto make_fixed_string_span()
	{ return fixed_basic_string_span<CharT, N>(); }

//template<typename CharT, size_type N>
//auto make_fixed_string_span(CharT* s1)
//	{ return fixed_basic_string_span<CharT, N>(s1, s1 + N); }

template<typename CharT, size_type N>
auto make_fixed_string_span(CharT(&s)[N])
	{ return fixed_basic_string_span<CharT, N>(s, s + N); }

template<typename CharT, size_type N>
auto make_fixed_string_span(std::array<CharT, N>& s)
	{ return make_fixed_string_span<CharT, N>(s.data(), s.data() + N); }

template<typename CharT, size_type N>
basic_string_span<CharT const> make_fixed_string_span(std::array<CharT, N> const& s)
	{ return make_fixed_string_span<CharT>(s, s + N); }


// string_span comparison operators

template<typename CharT1, typename CharT2, size_type N1, size_type N2, class = typename std::enable_if
	<is_std_char<CharT1>::value && is_std_char<CharT2>::value>::type>
constexpr bool operator<(span<CharT1, N1> const& l, CharT2 (&r)[N2])
	{ return std::lexicographical_compare(std::begin(l), std::end(l), std::begin(r), std::end(r) - 1); }

template<typename CharT1, typename CharT2, size_type N1, size_type N2, class = typename std::enable_if
<is_std_char<CharT1>::value && is_std_char<CharT2>::value>::type>
constexpr bool operator==(span<CharT1, N1> const& l, CharT2 (&r)[N2])
	{ return std::equal(std::begin(l), std::end(l), std::begin(r), std::end(r) - 1); }

template<typename CharT1, typename CharT2, size_type N1, size_type N2, class = typename std::enable_if
<is_std_char<CharT1>::value && is_std_char<CharT2>::value>::type>
constexpr bool operator!=(span<CharT1, N1> const& l, CharT2 (&r)[N2])
	{ return !(l == r); }

template<typename CharT1, typename CharT2, size_type N1, size_type N2, class = typename std::enable_if
<is_std_char<CharT1>::value && is_std_char<CharT2>::value>::type>
constexpr bool operator<=(span<CharT1, N1> const& l, CharT2 (&r)[N2])
	{ return l < r || l == r; }

template<typename CharT1, typename CharT2, size_type N1, size_type N2, class = typename std::enable_if
<is_std_char<CharT1>::value && is_std_char<CharT2>::value>::type>
constexpr bool operator>(span<CharT1, N1> const& l, CharT2 (&r)[N2])
	{ return !(l <= r); }

template<typename CharT1, typename CharT2, size_type N1, size_type N2, class = typename std::enable_if
<is_std_char<CharT1>::value && is_std_char<CharT2>::value>::type>
constexpr bool operator>=(span<CharT1, N1> const& l, CharT2 (&r)[N2])
	{ return l > r || l == r; }


template<typename CharT1, typename CharT2, size_type N1, size_type N2, class = typename std::enable_if
	<is_std_char<CharT1>::value && is_std_char<CharT2>::value>::type>
constexpr bool operator<(CharT1 (&l)[N1], span<CharT2, N2> const& r)
	{ return r >= l; }

template<typename CharT1, typename CharT2, size_type N1, size_type N2, class = typename std::enable_if
<is_std_char<CharT1>::value && is_std_char<CharT2>::value>::type>
constexpr bool operator==(CharT1 (&l)[N1], span<CharT2, N2> const& r)
	{ return r == l; }

template<typename CharT1, typename CharT2, size_type N1, size_type N2, class = typename std::enable_if
<is_std_char<CharT1>::value && is_std_char<CharT2>::value>::type>
constexpr bool operator!=(CharT1 (&l)[N1], span<CharT2, N2> const& r)
	{ return r != l; }

template<typename CharT1, typename CharT2, size_type N1, size_type N2, class = typename std::enable_if
<is_std_char<CharT1>::value && is_std_char<CharT2>::value>::type>
constexpr bool operator<=(CharT1 (&l)[N1], span<CharT2, N2> const& r)
	{ return r > l; }

template<typename CharT1, typename CharT2, size_type N1, size_type N2, class = typename std::enable_if
<is_std_char<CharT1>::value && is_std_char<CharT2>::value>::type>
constexpr bool operator>(CharT1 (&l)[N1], span<CharT2, N2> const& r)
	{ return r <= l; }

template<typename CharT1, typename CharT2, size_type N1, size_type N2, class = typename std::enable_if
<is_std_char<CharT1>::value && is_std_char<CharT2>::value>::type>
constexpr bool operator>=(CharT1 (&l)[N1], span<CharT2, N2> const& r)
	{ return r < l; }

// String functions (externalized)

template<typename CharT, size_type N,
	typename Traits = std::char_traits<typename std::remove_const<CharT>::type>,
		typename Alloc = std::allocator<typename std::remove_const<CharT>::type>>
auto to_string(span<CharT, N> sp)
{
	return std::basic_string<typename std::remove_const<CharT>::type, Traits, Alloc>(sp.data(), sp.size());
}

template<typename CharT>
basic_string_span<CharT> substr(basic_string_span<CharT> ss, index pos, index len)
{
	CORELIKE_ASSERT(pos <= index(ss.size()));
	return ss.subspan(pos, len);
//	return make_string_span(ss.data() + pos, std::min(ss.size() - pos, len));
}

template<typename CharT>
basic_string_span<CharT> substr(basic_string_span<CharT> ss, index pos)
{
	return ss.subspan(pos);
//	return substr(ss, pos, ss.size() - pos);
}

template<typename CharT>
CharT& at(basic_string_span<CharT> ss, index pos)
{
	CORELIKE_ASSERT(pos < index(ss.size()));
	return ss[pos];
}

} // namespace corelike
} // namespace galik

//namespace std {
//
//template<galik::corelike::size_type N>
//std::ostream& operator<<(std::ostream& os, galik::corelike::span<char, N> sp)
//	{ return os << std::string(sp.data(), sp.size()); }
//
//} // namespace std

#endif // CORELIKE_STRING_SPAN_HPP
