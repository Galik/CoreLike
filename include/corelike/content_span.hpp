#ifndef CORELIKE_CONTENT_SPAN_HPP
#define CORELIKE_CONTENT_SPAN_HPP
//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <array>
#include <iterator>
#include <vector>

#include "assert.hpp"
#include "span.hpp"
#include "span_algorithm.hpp" // remove if refactor content_span algorithms
#include "string_span.hpp"

namespace galik {
namespace corelike {

template<typename T, size_type N = dynamic_extent>
class content_span
: public span<T, N>
{
	using span_type = span<T, N>;
	using value_type = typename span_type::value_type;
	using mutable_value_type = typename std::remove_const<value_type>::type;
	using const_value_type = mutable_value_type const;
	using const_span_type = span<const_value_type>;

public:
//	explicit content_span(size_type, span_type sp): span_type(m_arr) { std::copy(std::begin(sp), std::end(sp), std::begin(m_arr)); }

	explicit content_span(): span_type(m_arr), m_arr({}) {}
	explicit content_span(size_type): span_type(m_arr), m_arr({}) {}
	explicit content_span(span_type sp): span_type(m_arr) { std::copy(std::begin(sp), std::end(sp), std::begin(m_arr)); }
	explicit content_span(std::array<mutable_value_type, N> const& a): span_type(m_arr), m_arr(a) {}

	operator span_type()             { return *this; }
	operator const_span_type() const { return *this; }

	void copy_remove(std::array<mutable_value_type, N>& a) const { a = m_arr; }
	void copy_assign(std::array<mutable_value_type, N> const& a) { m_arr = a; }

private:
	std::array<mutable_value_type, N> m_arr;
};

template<typename T>
class content_span<T, dynamic_extent>
: public span<T>
{
	using span_type = span<T>;
	using value_type = typename span_type::value_type;
	using mutable_value_type = typename std::remove_const<value_type>::type;
	using const_value_type = mutable_value_type const;
	using const_span_type = span<const_value_type>;

public:
//	explicit content_span(size_type, span_type sp): m_vec(std::begin(sp), std::end(sp))
//		{ static_cast<span_type&>(*this) = span_type(m_vec); }

	explicit content_span()
		{ static_cast<span_type&>(*this) = span_type(m_vec); }

	template<typename Iter>
	explicit content_span(Iter begin, Iter end): m_vec(begin, end)
		{ static_cast<span_type&>(*this) = span_type(m_vec); }
	explicit content_span(span<T> sp): content_span(std::begin(sp), std::end(sp)) {}
	explicit content_span(size_type n): m_vec(n) { static_cast<span_type&>(*this) = span_type(m_vec); }
	explicit content_span(std::vector<mutable_value_type>&& v): m_vec(std::move(v))
		{ static_cast<span_type&>(*this) = span_type(m_vec); }

	operator span_type()             { return *this; }
	operator const_span_type() const { return *this; }

	void move_remove(std::vector<mutable_value_type>& v)
	{
		v = std::move(m_vec);
		m_vec.clear();
		static_cast<span_type&>(*this) = span_type(m_vec);
	}

	void move_assign(std::vector<mutable_value_type>&& v)
	{
		m_vec = std::move(v);
		v.clear();
		static_cast<span_type&>(*this) = span_type(m_vec);
	}

private:
	std::vector<mutable_value_type> m_vec;
};

template<typename T, size_type N = dynamic_extent>
using const_content_span = content_span<typename std::remove_const<T>::type const, N>;

template<typename T, size_type N>
auto realize(span<T, N> sp)
	{ return content_span<T, N>(sp); }

/// Algorithms

template<typename T, size_type N>
auto copy(span<T, N> sp)
{
	content_span<T, N> csp(sp.size());
	std::copy(std::begin(sp), std::end(sp), std::begin(csp));
	return csp;
}

template<typename T, size_type N, typename UnaryPredicate>
auto copy_if(span<T, N> sp, UnaryPredicate p)
{
	std::vector<T> v(sp.size());
	v.erase(copy_if(sp, std::begin(v), p), std::end(v));
	return content_span<T>(v);
}

template<typename T, size_type N>
auto copy_backward(span<T, N> sp)
{
	content_span<T, N> csp(sp.size());
	copy_backward(sp, std::begin(csp));
	return csp;
}

template<typename T, size_type N>
auto move(span<T, N> sp)
{
	content_span<T, N> csp(sp.size());
	move(sp, std::begin(csp));
	return csp;
}

template<typename T, size_type N>
auto move_backward(span<T, N> sp)
{
	content_span<T, N> csp(sp.size());
	move_backward(sp, std::begin(csp));
	return csp;
}


template<typename Generator>
auto generate_n(size_type n, Generator g)
{
	content_span<typename std::remove_const<decltype(g())>::type> csp(n);
	generate(csp, g);
	return csp;
}

template<typename T, size_type N> inline
auto rotate_copy(split_span<T, N> sp)
{
	content_span<T, N> csp(sp.size());
	std::rotate_copy(std::begin(sp), sp.mid(), std::end(sp), std::begin(csp));
	return csp;
}

template<typename T, size_type N> inline
auto unique_copy(span<T, N> sp)
{
	content_span<T, N> csp(sp.size());
	std::unique_copy(std::begin(sp), std::end(sp), std::begin(csp));
	return csp;
}

template<typename T, size_type N, typename BinaryPredicate> inline
auto unique_copy(span<T, N> sp, BinaryPredicate p)
{
	content_span<T, N> csp(sp.size());
	std::unique_copy(std::begin(sp), std::end(sp), std::begin(csp), p);
	return csp;
}

template<typename T, size_type N, typename UnaryPredicate>
auto partition_copy(span<T, N> sp, UnaryPredicate p)
{
	std::vector<T> v1(sp.size());
	std::vector<T> v2(sp.size());
	auto pp = std::partition_copy(std::begin(sp), std::end(sp), std::begin(v1), std::begin(v2), p);
	v1.resize(std::distance(std::begin(v1), pp.first));
	v2.resize(std::distance(std::begin(v2), pp.second));
	return std::make_pair(content_span<T, N>(std::move(v1)), content_span<T, N>(std::move(v2)));
}

/// Other

// TODO: How to make these fixed_span friendly?

template<typename Iter>
auto make_content(Iter begin, Iter end)
{
	CORELIKE_ASSERT(std::distance(begin, end) >= 0);
	using value_type = typename std::remove_const<typename std::remove_reference<decltype(*begin)>::type>::type;
	content_span<value_type> csp(std::size_t(std::distance(begin, end)));
	std::copy(begin, end, std::begin(csp));
	return csp;
}

template<typename T>
auto make_content(std::initializer_list<T> il)
	{ return make_content(std::begin(il), std::end(il)); }

template<typename... Ts>
constexpr auto make_content(Ts... ts)
{
	using T = std::common_type_t<Ts...>;
    constexpr auto const N = sizeof...(Ts);

	std::array<T, N> arr{ts...};
	return copy(span<T, N>(arr));
}

template<typename CharT>
auto make_content(CharT* s, size_type n)
	{ return make_content(s, s + n); }

//template<typename CharT>
//auto make_content(CharT* s)
//	{ return make_content(s, std::find(s, s + std::numeric_limits<CharT*>::max(), CharT('\0'))); }

template<typename CharT, class = typename std::enable_if<is_std_char<CharT>::value>::type>
auto make_basic_string_content(size_type n)
	{ return content_span<CharT>(n); }

auto make_byte_content(size_type n)
	{ return make_basic_string_content<byte>(n); }

auto make_string_content(size_type n)
	{ return make_basic_string_content<char>(n); }

auto make_wstring_content(size_type n)
	{ return make_basic_string_content<wchar_t>(n); }

auto make_u16string_content(size_type n)
	{ return make_basic_string_content<char16_t>(n); }

auto make_u32string_content(size_type n)
	{ return make_basic_string_content<char32_t>(n); }

template<typename CharT, typename Traits, typename Alloc>
auto make_string_content(std::basic_string<CharT, Traits, Alloc> const& s)
	{ return make_content(std::begin(s), std::end(s)); }

//template<typename CharT, size_type N>
//auto make_content(CharT(&s)[N])
//	{ return make_content(s, s + N); }

template<typename T>
content_span<span<T>> divide_span(span<T> sp, size_type n)
{
	CORELIKE_ASSERT(n > 0);

	auto len = sp.size() / n;
	auto rem = sp.size() % n;

	content_span<span<T>> spans(n);

	spans[0] = span<T>(sp.data(), len + (0 < rem));
	for(auto i = 1UL; i < n; ++i)
		spans[i] = span<T>(std::end(spans[i - 1]), len + (i < rem));

	return spans;
}

} // namespace corelike
} // namespace galik

#endif // CORELIKE_CONTENT_SPAN_HPP
