#ifndef CORELIKE_NARROW_CAST_HPP
#define CORELIKE_NARROW_CAST_HPP
//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <limits>
#include <type_traits>

#include "assert.hpp"

namespace galik {
namespace corelike {

template <class T, class U>
inline T narrow_cast(U u) CORELIKE_NOEXCEPT
{
	T t = static_cast<T>(u);
	CORELIKE_ASSERT_MSG(static_cast<U>(t) == u, "narrowing changed the value from: " << std::size_t(u) << " to: " << std::size_t(t));
	CORELIKE_ASSERT_MSG((std::is_signed<T>::value == std::is_signed<U>::value) || (t < T{}) == (u < U{}),
		"narrowing changed the sign of the value from: " << std::size_t(u) << " to: " << std::size_t(t));
	return t;
}

template <class I, class F>
inline I floating_to_integral(F u) CORELIKE_NOEXCEPT
{
	static_assert(std::is_floating_point<F>::value,
		"Can only be used to cast from floating types.");

	static_assert(std::is_integral<I>::value,
		"Can only be used to cast to integral types.");

	CORELIKE_ASSERT_MSG(F(std::numeric_limits<I>::max()) >= u && F(std::numeric_limits<I>::lowest()) <= u,
		"target type is not large enough to contain the source value: " << u);

	return static_cast<I>(u);
}

} // namespace corelike
} // namespace galik

#endif // CORELIKE_NARROW_CAST_HPP
