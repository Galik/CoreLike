#ifndef CORELIKE_SPAN_ALLOCATOR_HPP
#define CORELIKE_SPAN_ALLOCATOR_HPP
//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <algorithm>
#include <cassert>
#include <memory>
#include <vector>

#if __cplusplus < 201703
#  include <experimental/optional>
#else
#  include <optional>
#endif

#include "assert.hpp"
#include "span.hpp"

namespace galik {
namespace corelike {

#if __cplusplus < 201703
	template<typename T>
	using optional = std::experimental::optional<T>;
#else
	template<typename T>
	using optional = std::optional<T>;
#endif

template<typename T>
class span_allocator
{
public:
	using value_type = T;
	using span_type = span<value_type>;
	using optional_span = optional<span_type>;

	span_allocator(size_type n)
	: m_ptr(std::make_unique<std::vector<value_type>>(n))
	, m_free(1, span_type{m_ptr->data()
	, index(m_ptr->size())})
	{
	}

	span_allocator(std::unique_ptr<std::vector<value_type>> ptr)
	: m_ptr(std::move(ptr))
	, m_free(1, span_type{m_ptr->data()
	, index(m_ptr->size())})
	{
		if(!m_ptr)
			throw std::runtime_error("null std::unique_ptr passed to span_allocator constructor");
	}

	void resize(size_type n)
	{
		m_ptr->clear();
		m_used.clear();
		m_free.clear();
		m_ptr->resize(n);
	}

	std::vector<span_type> divide_into(index n)
	{
		m_used.clear();
		m_free.clear();
		std::vector<span_type> sps;

		auto sz = (m_ptr->size() / n) + (m_ptr->size() % n);

		while(auto sp = allocate(sz))
			sps.push_back(sp.value());

		return sps;
	}

	// TODO: Make this policy plugable
	optional<span_type> allocate(index n)
	{
		CORELIKE_ASSERT(n > 0);
		CORELIKE_ASSERT(n <= index(m_ptr->size()));

		auto found = std::find_if(std::begin(m_free), std::end(m_free), [n](span_type s){
			return n == s.size();
		});

		if(found != std::end(m_free))
		{
			m_used.push_back(*found);
			m_free.erase(found);
			return m_used.back();
		}

		found = std::find_if(std::begin(m_free), std::end(m_free), [n](span_type s){
			return n < s.size();
		});

		if(found == std::end(m_free))
			return {};

		m_used.push_back(found->subspan(0, n));
		*found = found->subspan(n);
		return m_used.back();
	}

	void deallocate(span_type s)
	{
		auto found = std::find_if(std::begin(m_used), std::end(m_used), [s](span_type s1){
			return s.data() == s1.data();
		});

		CORELIKE_ASSERT(found != std::end(m_used));

		move_to_free(found);
	}

	optional<span_type> reallocate(span_type s, size_type n)
	{
		if(n <= s.size())
			return s.subspan(0, n);

		if(auto o = allocate(s.size()))
		{
			std::move(std::begin(s), std::end(s), std::begin(o.value()));
			return o;
		}

		return {};
	}

	void defragment_automatically() { m_auto_defrag = true; }
	void do_not_defragment_automatically() { m_auto_defrag = false; }

	void defragment()
	{
		if(m_auto_defrag || m_free.empty())
			return;

		std::sort(std::begin(m_free), std::end(m_free), [](span_type a, span_type b){
			return a.data() < b.data();
		});

		for(auto s = std::begin(m_free); s != std::prev(std::end(m_free)); ++s)
		{
			if(s->data() + s->size() != std::next(s)->data())
				continue;

			*std::next(s) = span_type(s->data(), s->size() + std::next(s)->size());
			*s = span_type(s->data(), s->data());
		}

		m_free.erase(std::remove_if(std::begin(m_free), std::end(m_free), [](span_type s){
			return s.empty();
		}), std::end(m_free));
	}

//	void dump()
//	{
//		std::cout << "m_data: " << m_ptr->size() << '\n';
//		int i = 0;
//		for(auto sp: m_data)
//			std::cout << i++ << ": " << sp << '\n';
//
//		std::cout << "m_used: " << m_used.size() << '\n';
//		for(auto sp: m_used)
//			print(sp);
//
//		std::cout << "m_free: " << m_free.size() << '\n';
//		for(auto sp: m_free)
//			print(sp);
//	}

	std::vector<index> state()
	{
		std::vector<index> s;
		// used_size, {used pos}*, free_size, {free pos}*
		{
			auto temp = m_used;
			s.push_back(temp.size());
			std::transform(std::begin(temp), std::end(temp), std::back_inserter(s), [this](span_type sp){
				return std::distance(m_ptr->data(), sp.data());
			});
		}
		{
			auto temp = m_free;
			s.push_back(temp.size());
			std::transform(std::begin(temp), std::end(temp), std::back_inserter(s), [this](span_type sp){
				return std::distance(m_ptr->data(), sp.data());
			});
		}
		return s;
	}

	value_type* data() { return m_ptr->data(); }
	value_type const* data() const { return m_ptr->data(); }

private:

//	void print(span_type sp)
//	{
//		std::cout << '{' << std::distance(m_ptr->data(), sp.data())
//			<< ", " << std::distance(m_ptr->data(), sp.data() + sp.size()) << '}' << '\n';
//	}

	void move_to_free(typename std::vector<span_type>::iterator found)
	{
		if(!m_auto_defrag)
			m_free.push_back(*found);
		else
		{
			auto lb = std::lower_bound(std::begin(m_free), std::end(m_free), *found);

			if(lb == std::end(m_free))
				lb = m_free.insert(lb, *found);
			else
			{
				if(found->data() + found->size() == lb->data())
					*lb = span_type(found->data(), found->size() + lb->size());
				else
					lb = m_free.insert(lb, *found);
			}

			if(lb != std::begin(m_free))
			{
				auto prev = std::prev(lb);

				if(prev->data() + prev->size() == lb->data())
				{
					*prev = span_type(prev->data(), prev->size() + lb->size());
					lb = m_free.erase(lb);
				}
			}
		}

		m_used.erase(found);
	}

	bool m_auto_defrag = false; // if true, deallocate => OlogN, else O1
	std::unique_ptr<std::vector<value_type>> m_ptr;
	std::vector<span_type> m_used;
	std::vector<span_type> m_free;
};

} // namespace corelike
} // namespace galik

#endif // CORELIKE_SPAN_ALLOCATOR_HPP

