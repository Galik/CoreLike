#ifndef CORELIKE_THREAD_H
#define CORELIKE_THREAD_H
//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <algorithm>
#include <future>
#include <mutex>
#include <queue>
#include <shared_mutex>
#include <string>
#include <thread>
#include <vector>
#include <cstddef> // std::size_t

// for examples
#include <random>

//#include "span.hpp"

#undef CORELIKE_NODISCARD
#if __has_cpp_attribute(nodiscard)
#define CORELIKE_NODISCARD [[nodiscard]]
#else
#ifdef __GNUC__
#define CORELIKE_NODISCARD [[gnu::warn_unused_result]]
#else
#define CORELIKE_NODISCARD
#endif
#endif // __has_cpp_attribute(nodiscard)

namespace galik {
namespace corelike {

// types

class joining_thread
{
public:
	joining_thread() noexcept = default;

	joining_thread(joining_thread const&) = delete;
	joining_thread(joining_thread&& other) noexcept
	: t(std::move(other.t)) {}

	joining_thread(std::thread const&) = delete;
	joining_thread(std::thread&& other) noexcept
	: t(std::move(other)) {}

	joining_thread& operator=(joining_thread const&) = delete;
	joining_thread& operator=(joining_thread&& other) noexcept
		{ t = std::move(other.t); return *this; }

	joining_thread& operator=(std::thread const&) = delete;
	joining_thread& operator=(std::thread&& other) noexcept
		{ t = std::move(other); return *this; }

	template<typename Callable, typename... Args>
	explicit joining_thread(Callable&& f, Args&&... args)
		: t(std::forward<Callable>(f), std::forward<Args>(args)...) {}

	~joining_thread() { join(); }

	void join() { if(t.joinable()) t.join(); }

	std::thread::id get_id() const noexcept { return t.get_id(); }

	std::thread::native_handle_type native_handle()
		{ return t.native_handle(); }

	void swap(joining_thread& other) noexcept
		{ using std::swap; swap(t, other.t); }

	bool free() { return !t.joinable(); }

private:
	std::thread t;

	friend void swap(joining_thread& t1, joining_thread& t2) noexcept
	{
		using std::swap;
		swap(t1.t, t2.t);
	}
};

class detached_thread
{
public:
	detached_thread() noexcept = default;

	detached_thread(detached_thread const&) = delete;
	detached_thread(detached_thread&& other) noexcept
	: t(std::move(other.t)) {}

	detached_thread(std::thread const&) = delete;
	detached_thread(std::thread&& other) noexcept
	: t(std::move(other)) {}

	detached_thread& operator=(detached_thread const&) = delete;
	detached_thread& operator=(detached_thread&& other) noexcept
		{ t = std::move(other.t); return *this; }

	detached_thread& operator=(std::thread const&) = delete;
	detached_thread& operator=(std::thread&& other) noexcept
		{ t = std::move(other); return *this; }

	template<typename Callable, typename... Args>
	explicit detached_thread(Callable&& f, Args&&... args)
		: t(std::forward<Callable>(f), std::forward<Args>(args)...) { t.detach(); }

	~detached_thread() {}

	std::thread::id get_id() const noexcept { return t.get_id(); }

	std::thread::native_handle_type native_handle()
		{ return t.native_handle(); }

	void swap(detached_thread& other) noexcept
		{ using std::swap; swap(t, other.t); }

private:
	std::thread t;

	friend void swap(detached_thread& t1, detached_thread& t2) noexcept
	{
		using std::swap;
		swap(t1.t, t2.t);
	}
};

class parallel_jobs
: private std::vector<joining_thread>
{
public:
	using std::vector<joining_thread>::reserve;
	using std::vector<joining_thread>::size;

	template<typename Callable, typename... Args>
	void add(Callable&& callable, Args&&... args)
		{ emplace_back(std::forward<Callable>(callable), std::forward<Args>(args)...); }

	void wait() { clear(); }
};

template<typename... Funcs> inline
void parallel_invoke(Funcs... funcs)
{
	joining_thread array[]{joining_thread{funcs}...};
}

// For dividing work up among threads

//=============================================================
//== Locked objects
//=============================================================
//
// Locked Objects that can be opened for reading or writing
//

/***
 * Trying to *copy* or *move* a locked_object while a open_for_reading or open_for_writing
 * will cause *deadlock*.
 */

template<typename Object, typename Mutex>
class locked_object_base
{
public:
	using mutex_type = Mutex;
	using unique_lock = std::unique_lock<Mutex>;
	using shared_lock = typename std::conditional
	<
		std::is_same<Mutex, std::mutex>::value,
		std::unique_lock<Mutex>,
		std::shared_lock<Mutex>
	>::type;

	locked_object_base() = default;
	virtual ~locked_object_base() = default;

	template<typename... Args>
	locked_object_base(Args&&... args): object(std::forward<Args>(args)...) {}

	locked_object_base(locked_object_base const& other)
	{
		unique_lock lock(other.mtx);
		copy_assign(other);
	}

	locked_object_base(locked_object_base&& other)
	{
		unique_lock lock(other.mtx);
		move_assign(other);
	}

	locked_object_base& operator=(locked_object_base const& other)
	{
		unique_lock lock(other.mtx);
		copy_assign(other);
		return *this;
	}

	locked_object_base& operator=(locked_object_base&& other)
	{
		unique_lock lock(other.mtx);
		move_assign(other);
		return *this;
	}

	mutex_type& mutex() const { return mtx; }

protected:
	void copy_assign(locked_object_base const& other) { this->object = other.object; }
	void move_assign(locked_object_base&& other) { this->object = std::move(other.object); }

	mutable mutex_type mtx;
	Object object;
};

template<typename Object, typename Mutex>
class readable_locked_object
: public virtual locked_object_base<Object, Mutex>
{
public:
	using mutex_type = typename locked_object_base<Object, Mutex>::mutex_type;
	using unique_lock = typename locked_object_base<Object, Mutex>::unique_lock;
	using shared_lock = typename locked_object_base<Object, Mutex>::shared_lock;

	class reading_accessor
	{
	public:
		reading_accessor() = default;

		reading_accessor(Object const& object, mutex_type& mtx)
			: object(&object), lock(mtx) {}

		reading_accessor(Object const& object, mutex_type& mtx, std::defer_lock_t)
			: object(&object), lock(mtx, std::defer_lock) {}

		// it should be impossible for a reading_lock to exist without an associated
		// locked mutex
		reading_accessor(reading_accessor const& other)
			: object(other.object), lock(other.lock.mtx) {}

		reading_accessor(reading_accessor&& other)
			: object(other.object), lock(std::move(other.lock)) {}

		reading_accessor& operator=(reading_accessor const& other)
		{
			lock = shared_lock(other.lock.mtx);
			object = other.object;
			return *this;
		}

		reading_accessor& operator=(reading_accessor&& other)
		{
			lock = std::move(other.lock);
			object = other.object;
			return *this;
		}

		Object const& operator*() const { return *object; }
		Object const* operator->() const { return object; }

		shared_lock& get_lock() { return lock; }

	private:
		Object const* object;
		shared_lock lock;
	};

	readable_locked_object() = default;

	template<typename... Args>
	readable_locked_object(Args&&... args)
		: locked_object_base<Object, Mutex>(std::forward<Args>(args)...) {}

	readable_locked_object(readable_locked_object const& other)
		: locked_object_base<Object, Mutex>(other) {}

	readable_locked_object(readable_locked_object&& other)
		: locked_object_base<Object, Mutex>(std::move(other)) {}

	readable_locked_object& operator=(readable_locked_object const& other)
	{
		unique_lock lock(other.mtx);
		locked_object_base<Object, Mutex>::copy_assign(other);
		copy_assign(other);
		return *this;
	}

	readable_locked_object& operator=(readable_locked_object&& other)
	{
		unique_lock lock(other.mtx);
		locked_object_base<Object, Mutex>::move_assign(other);
		move_assign(other);
		return *this;
	}

	CORELIKE_NODISCARD
	reading_accessor open_for_reading() const
	{
		return reading_accessor(locked_object_base<Object, Mutex>::object,
			locked_object_base<Object, Mutex>::mtx);
	}

	CORELIKE_NODISCARD
	reading_accessor open_for_deferred_reading() const
	{
		return reading_accessor(locked_object_base<Object, Mutex>::object,
			locked_object_base<Object, Mutex>::mtx, std::defer_lock);
	}

protected:
	void copy_assign(readable_locked_object const& other) {}
	void move_assign(readable_locked_object&& other) {}
};

template<typename Object, typename Mutex>
class writable_locked_object
: public virtual locked_object_base<Object, Mutex>
{
public:
	using mutex_type = typename locked_object_base<Object, Mutex>::mutex_type;
	using unique_lock = typename locked_object_base<Object, Mutex>::unique_lock;
	using shared_lock = typename locked_object_base<Object, Mutex>::shared_lock;

	class writing_accessor
	{
	public:
		writing_accessor() = default;

		writing_accessor(Object& object, mutex_type& mtx): object(&object), lock(mtx) {}

		writing_accessor(Object& object, mutex_type& mtx, std::defer_lock_t)
			: object(&object), lock(mtx, std::defer_lock) {}

		writing_accessor(writing_accessor const&) = delete;
		writing_accessor(writing_accessor&& other): object(other.object), lock(std::move(other.lock)) {}

		writing_accessor& operator=(writing_accessor const&) = delete;
		writing_accessor& operator=(writing_accessor&& other)
		{
			lock = std::move(other.lock);
			object = other.object;
			return *this;
		}

		Object& operator*() { return *object; }
		Object* operator->() { return object; }

		unique_lock& get_lock() { return lock; }

	private:
		Object* object;
		unique_lock lock;
	};

	writable_locked_object() = default;

	template<typename... Args>
	writable_locked_object(Args&&... args): locked_object_base<Object, Mutex>(std::forward<Args>(args)...) {}
	writable_locked_object(writable_locked_object const& other): locked_object_base<Object, Mutex>(other) {}
	writable_locked_object(writable_locked_object&& other): locked_object_base<Object, Mutex>(std::move(other)) {}

	writable_locked_object& operator=(writable_locked_object const& other)
	{
		unique_lock lock(other.mtx);
		locked_object_base<Object, Mutex>::copy_assign(other);
		copy_assign(other);
		return *this;
	}

	writable_locked_object& operator=(writable_locked_object&& other)
	{
		unique_lock lock(other.mtx);
		locked_object_base<Object, Mutex>::move_assign(other);
		move_assign(other);
		return *this;
	}

	CORELIKE_NODISCARD
	writing_accessor open_for_writing()
	{
		return writing_accessor(locked_object_base<Object, Mutex>::object,
			locked_object_base<Object, Mutex>::mtx);
	}

	CORELIKE_NODISCARD
	writing_accessor open_for_deferred_writing()
	{
		return writing_accessor(locked_object_base<Object, Mutex>::object,
			locked_object_base<Object, Mutex>::mtx, std::defer_lock);
	}

protected:
	void copy_assign(writable_locked_object const& other) {}
	void move_assign(writable_locked_object&& other) {}
};

template<typename Object, typename Mutex = std::shared_timed_mutex>
class locked_object
: public readable_locked_object<Object, Mutex>
, public writable_locked_object<Object, Mutex>
{
public:
	using mutex_type = typename locked_object_base<Object, Mutex>::mutex_type;
	using unique_lock = typename locked_object_base<Object, Mutex>::unique_lock;
	using shared_lock = typename locked_object_base<Object, Mutex>::shared_lock;

	template<typename... Args>
	locked_object(Args&&... args): locked_object_base<Object, Mutex>(std::forward<Args>(args)...) {}
	locked_object(locked_object const& other): locked_object_base<Object, Mutex>(other) {}
	locked_object(locked_object&& other): locked_object_base<Object, Mutex>(std::move(other)) {}

	locked_object& operator=(locked_object const& other)
	{
		unique_lock lock(other.mtx);
		locked_object_base<Object, Mutex>::copy_assign(other);
		readable_locked_object<Object, Mutex>::copy_assign(other);
		writable_locked_object<Object, Mutex>::copy_assign(other);
		copy_assign(other);
		return *this;
	}

	locked_object& operator=(locked_object&& other)
	{
		unique_lock lock(other.mtx);
		locked_object_base<Object, Mutex>::move_assign(other);
		readable_locked_object<Object, Mutex>::move_assign(other);
		writable_locked_object<Object, Mutex>::move_assign(other);
		move_assign(other);
		return *this;
	}

protected:
	void copy_assign(locked_object const& other) {}
	void move_assign(locked_object&& other) {}
};

namespace detail {

	template<typename Tuple, std::size_t... Is>
	void apply_multilock(Tuple& tp, std::index_sequence<Is...>)
		{ std::lock(std::get<Is>(tp).get_lock()...); }

	template<typename Object, typename Mutex>
	struct for_reading_type
	{
		locked_object<Object, Mutex> const& l;
		for_reading_type(locked_object<Object, Mutex> const& l): l(l) {}
		auto lock() { return l.open_for_deferred_reading(); }
	};

	template<typename Object, typename Mutex>
	struct for_writing_type
	{
		locked_object<Object, Mutex>& l;
		for_writing_type(locked_object<Object, Mutex>& l): l(l) {}
		auto lock() { return l.open_for_deferred_writing(); }
	};

} // namespace detail

template<typename Object, typename Mutex>
auto for_reading(locked_object<Object, Mutex> const& l)
	{ return detail::for_reading_type<Object, Mutex>(l); }

template<typename Object, typename Mutex>
auto for_writing(locked_object<Object, Mutex>& l)
	{ return detail::for_writing_type<Object, Mutex>(l); }

template<typename... Lockables>
CORELIKE_NODISCARD
auto open_locked_objects(Lockables&&... lockables)
{
	auto tp = std::make_tuple(std::forward<Lockables>(lockables).lock()...);
	detail::apply_multilock(tp, std::make_index_sequence<sizeof...(Lockables)>{});
	return tp;
}

//=============================================================
//== Thread pool
//=============================================================
//

class thread_pool
{
public:
	thread_pool() = default;
	thread_pool(thread_pool const&) = delete;
	thread_pool& operator=(thread_pool const&) = delete;

	~thread_pool() { stop(); }

	template<typename Func, typename... Params>
	void add(Func func, Params&&... params)
	{
		if(closing_down||done)
			throw std::runtime_error("adding job to dead pool");

		{
			std::unique_lock<std::mutex> lock(mtx);
//			jobs.push([=]{ func(std::forward<Params>(params)...); });
			jobs.push([func, &params...]{ func(std::forward<Params>(params)...); });
		}

		cv.notify_all();
	}

	std::size_t size() const
	{
		std::unique_lock<std::mutex> lock(mtx);
		return threads.size();
	}

	void start(unsigned size = std::thread::hardware_concurrency())
	{
		{
			std::unique_lock<std::mutex> lock(mtx);
			cv.wait(lock, [this]{ return !closing_down; });
		}

		if(!done)
			throw std::runtime_error("trying to start a running pool, must close first");

		actual_start(size);
	}

	//! Prevent new jobs
	//! Wait for jobs to complete
	//! remove threads
	//! TODO: add timeout?
	void stop()
	{
		bool expected = false;
		if(!closing_down.compare_exchange_strong(expected, true))
			return;

		{
			std::unique_lock<std::mutex> lock(mtx);
			cv.wait(lock, [this]{ return jobs.empty(); });
		}

		done = true;
		cv.notify_all();

		for(auto& thread: threads)
			thread.join();

		{
			std::unique_lock<std::mutex> lock(mtx);
			threads.clear();
		}

		closing_down = false;
		cv.notify_all();
	}

	void wait() // wait for current cueue to empty
	{
		std::unique_lock<std::mutex> lock(mtx);
		cv.wait(lock, [this]{ return jobs.empty(); });
	}

private:
	void actual_start(unsigned size)
	{
		{
			std::unique_lock<std::mutex> lock(mtx);
			while(size--)
				threads.emplace_back(&thread_pool::process, this);
		}

		done = false;
		cv.notify_all();
	}

	void process()
	{
		std::function<void()> func;

		while(!done)
		{
			{
				std::unique_lock<std::mutex> lock(mtx);
				cv.wait(lock, [this]{ return done || !jobs.empty(); });

				if(done)
					break;

				func = std::move(jobs.front());
				jobs.pop();
			}

			cv.notify_all();

			if(func)
				func();
		}
	}

	mutable std::mutex mtx;
	std::condition_variable cv;

	//! block queue and wait for it to empty
	std::atomic_bool closing_down{false};

	//! signal threads to end
	std::atomic_bool done{true};

	std::vector<std::thread> threads;
	std::queue<std::function<void()>> jobs;
};

//=============================================================
//== EXPERIMENTSL Predicate Variable
//=============================================================
//

class predicate_variable
{
public:
	predicate_variable() {}
	~predicate_variable() { stop(); }

	template<typename Predicate, typename Callable>
	void call_on_predicate(Predicate pred, Callable call)
	{
		m_jobs.add([&]{
			while(!m_done)
			{
				auto lock = obtain();
				m_cv.wait(lock, [&]{ return m_done || pred(); });
				if(!m_done)
					call();
				m_cv.notify_all();
			}
		});
	}

	void signal()
	{
		std::unique_lock<std::mutex> lock(m_mtx);
		m_cv.notify_all();
	}

	void stop()
	{
		m_done = true;
		m_cv.notify_all();
		m_jobs.wait();
	}

	std::unique_lock<std::mutex> obtain() { return std::unique_lock<std::mutex>(m_mtx); }

//	std::mutex& mutex() { return m_mtx; }

private:
	std::mutex m_mtx;
	std::condition_variable m_cv;
	std::atomic_bool m_done{false};
	parallel_jobs m_jobs;
};

template<typename T>
class atomic_queue
{
public:
	atomic_queue()
	{
		m_sv.call_on_predicate([&]{
			return !m_q.empty();
		}, [&]{
			while(!m_q.empty())
			{
				std::cout << ' ' << m_q.front();
				m_q.pop();
			}
		});
	}

	~atomic_queue() { m_sv.stop(); }

	void push(T const& value)
	{
		auto lock = m_sv.obtain();
		m_q.push(value);
		m_sv.signal();
	}

private:
	std::queue<T> m_q;
	predicate_variable m_sv;
};

// Dividing a span into multiple equal parts

// Moved to content_span !!!!!!!!!!!!

//template<typename T>
//std::vector<span<T>> divide_span(span<T> sp, size_type n)
//{
//	CORELIKE_ASSERT(n > 0);
//
//	auto len = sp.size() / n;
//	auto rem = sp.size() % n;
//
//	std::vector<span<T>> spans;
//	spans.reserve(n);
//
//	for(auto i = 0UL; i < n; ++i)
//		spans.emplace_back(sp.data() + (i * len), len + (i < rem));
//
//	return spans;
//}

} // corelike
} // galik

#endif // CORELIKE_THREAD_H
