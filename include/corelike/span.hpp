#ifndef CORELIKE_SPAN_HPP
#define CORELIKE_SPAN_HPP
//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include "assert.hpp"

#include <array>
#include <limits>
#include <vector>

#include <iostream> // debug REMOVE ME

#if __cplusplus >= 201703
#include <cstddef> // std::byte
#endif

namespace galik {
namespace corelike {

#if __cplusplus >= 201703
using byte = std::byte;
#else
enum class byte: unsigned char {};
#endif

using size_type = std::size_t;
using index = size_type;

constexpr const auto dynamic_extent = std::numeric_limits<size_type>::max();

template<typename T, size_type N>
class split_span;

namespace detail {

template<typename T, size_type N>
class span_impl
{
public:
	using value_type = T;
	using const_value_type = typename std::remove_const<value_type>::type const;
	using pointer = value_type*;
	using const_pointer = const_value_type*;

	void set_beg(pointer ptr) { m_beg = ptr; }
	void set_end(pointer) {}
	void clear() { set_beg(nullptr); set_end(nullptr); }

	constexpr pointer beg_ptr() { return m_beg; }
	constexpr const_pointer beg_ptr() const { return m_beg; }

	constexpr pointer end_ptr() { return m_beg + N; }
	constexpr const_pointer end_ptr() const { return m_beg + N; }

	constexpr size_type size() const { return N; }

private:
	pointer m_beg = nullptr;
};

template<typename T>
class span_impl<T, dynamic_extent>
{
public:
	using value_type = T;
	using const_value_type = typename std::remove_const<value_type>::type const;
	using pointer = value_type*;
	using const_pointer = const_value_type*;

	void set_beg(pointer ptr) { m_beg = ptr; }
	void set_end(pointer ptr) { m_end = ptr; }
	void clear() { set_beg(nullptr); set_end(nullptr); }

	constexpr pointer beg_ptr() { return m_beg; }
	constexpr const_pointer beg_ptr() const { return m_beg; }

	constexpr pointer end_ptr() { return m_end; }
	constexpr const_pointer end_ptr() const { return m_end; }

	constexpr size_type size() const { return size_type(m_end - m_beg); }

private:
	pointer m_beg = nullptr;
	pointer m_end = nullptr;
};

} // namespace detail

template<typename T, size_type N = dynamic_extent>
class span
{
public:
	friend class split_span<T, N>;

public:
	using value_type = T;
	using const_value_type = typename std::remove_const<value_type>::type const;

	using pointer = value_type*;
	using const_pointer = const_value_type*;

	using iterator = pointer;
	using const_iterator = const_pointer;

	using reverse_iterator = std::reverse_iterator<iterator>;
	using const_reverse_iterator = std::reverse_iterator<const_iterator>;

	using reference = value_type&;
	using const_reference = const_value_type&;

	explicit constexpr span() CORELIKE_NOEXCEPT {}

	explicit constexpr span(pointer beg, pointer end) CORELIKE_NOEXCEPT
		{ CORELIKE_ASSERT(beg <= end); m_impl.set_beg(beg); m_impl.set_end(end); }

	explicit constexpr span(pointer beg, size_type len) CORELIKE_NOEXCEPT
	: span(beg, beg + len) {}

	template<size_type NN>
	explicit constexpr span(value_type (&arr)[NN]) CORELIKE_NOEXCEPT
	: span(NN ? arr:nullptr, NN ? arr + NN:nullptr) {}

	template<size_type NN>
	explicit constexpr span(std::array<std::remove_const_t<value_type>, NN>& arr) CORELIKE_NOEXCEPT
	: span(arr.data(), arr.data() + NN) {}

	explicit constexpr span(std::array<std::remove_const_t<value_type>, 0>&) CORELIKE_NOEXCEPT
	: span(nullptr, nullptr) {}

	template<size_type NN>
	explicit constexpr span(std::array<std::remove_const_t<value_type>, NN> const& arr) CORELIKE_NOEXCEPT
	: span(arr.data(), arr.data() + NN) {}

	explicit constexpr span(std::array<std::remove_const_t<value_type>, 0> const&) CORELIKE_NOEXCEPT
	: span(nullptr, nullptr) {}

	template<typename Container, typename = typename std::enable_if<
		std::is_convertible<decltype(Container().data()), pointer>::value
	>::type>
	explicit constexpr span(Container& c) CORELIKE_NOEXCEPT
	: span(c.empty() ? nullptr:c.data(), c.empty() ? nullptr:c.data() + c.size()) {}

	template<typename Container, typename = typename std::enable_if<
		std::is_convertible<decltype(Container().data()), pointer>::value
	>::type>
	explicit constexpr span(Container const& c) CORELIKE_NOEXCEPT
	: span(c.empty() ? nullptr:c.data(), c.empty() ? nullptr:c.data() + c.size()) {}

	constexpr span(span const& other) CORELIKE_NOEXCEPT: m_impl(other.m_impl) {}

	enum class tag1{};
	enum class tag2{};

	template<typename TT, size_type NN, class X = typename std::enable_if<NN == N, tag1>::type>
	constexpr span(span<TT, NN> other, tag1 = {}) CORELIKE_NOEXCEPT: span(other.data(), other.data() + other.size())
		{}

	template<typename TT, size_type NN, class X = typename std::enable_if<
		   NN != N && N == dynamic_extent, tag2>::type>
	constexpr span(span<TT, NN> other, tag2 = {}) CORELIKE_NOEXCEPT: span(other.data(), other.data() + other.size())
		{}

	constexpr span& operator=(span const& other) = default;
	constexpr span& operator=(span&& other) CORELIKE_NOEXCEPT
	{
		m_impl.set_beg(other.m_impl.beg_ptr());
		m_impl.set_end(other.m_impl.end_ptr());
		other.m_impl.set_beg(nullptr);
		other.m_impl.set_end(nullptr);
		return *this;
	}

	constexpr size_type size() const { return m_impl.size(); }
	constexpr bool empty() const { return !bool(size()); }

	value_type& front() { CORELIKE_ASSERT(!empty()); return *m_impl.beg_ptr(); }
	const_value_type& front() const { CORELIKE_ASSERT(!empty()); return *m_impl.beg_ptr(); }

	value_type& back() { CORELIKE_ASSERT(!empty()); return *(m_impl.end_ptr() - 1); }
	const_value_type& back() const { CORELIKE_ASSERT(!empty()); return *(m_impl.end_ptr() - 1); }

	iterator begin() { return m_impl.beg_ptr(); }
	iterator end()   { return m_impl.end_ptr(); }

	const_iterator begin() const { return m_impl.beg_ptr(); }
	const_iterator end()   const { return m_impl.end_ptr(); }

	const_iterator cbegin() const { return m_impl.beg_ptr(); }
	const_iterator cend()   const { return m_impl.end_ptr(); }

	reverse_iterator rbegin() { return reverse_iterator(m_impl.end_ptr()); }
	reverse_iterator rend()   { return reverse_iterator(m_impl.beg_ptr()); }

	const_reverse_iterator rbegin() const { return reverse_iterator(m_impl.end_ptr()); }
	const_reverse_iterator rend()   const { return reverse_iterator(m_impl.beg_ptr()); }

	const_reverse_iterator crbegin() const { return reverse_iterator(m_impl.end_ptr()); }
	const_reverse_iterator crend()   const { return reverse_iterator(m_impl.beg_ptr()); }

	reference operator[](size_type n)
		{ CORELIKE_ASSERT(n < size()); return m_impl.beg_ptr()[n]; }

	const_reference operator[](size_type n) const
		{ CORELIKE_ASSERT(n < size()); return m_impl.beg_ptr()[n]; }

	reference at(size_type n)
	{
		if(!(n < size()))
			throw std::invalid_argument("bad subscript: " + std::to_string(n)
				+ " in span(0-" + std::to_string(size() - 1) + ")");
		return m_impl.beg_ptr()[n];
	}

	const_reference at(size_type n) const
	{
		if(!(n < size()))
			throw std::invalid_argument("bad subscript: " + std::to_string(n)
				+ " in span(0-" + std::to_string(size() - 1) + ")");
		return m_impl.beg_ptr()[n];
	}

	span subspan(size_type pos, size_type n)
	{
		CORELIKE_ASSERT(pos <= size());
		n = std::min(n, size() - pos);
		return span(m_impl.beg_ptr() + pos, m_impl.beg_ptr() + pos + n);
	}

	span<const_value_type, N> subspan(size_type pos, size_type n) const
	{
		CORELIKE_ASSERT(pos <= size());
		n = std::min(n, size() - pos);
		return span(m_impl.beg_ptr() + pos, m_impl.beg_ptr() + pos + n);
	}

	span subspan(size_type pos)
	{
		CORELIKE_ASSERT(pos <= size());
		return span(m_impl.beg_ptr() + pos, m_impl.end_ptr());
	}

	span<const_value_type, N> subspan(size_type pos) const
	{
		CORELIKE_ASSERT(pos <= size());
		return span<const_value_type, N>(m_impl.beg_ptr() + pos, m_impl.end_ptr());
	}

	template<size_type Pos, size_type Size = dynamic_extent>
	constexpr span subspan() const
	{
		CORELIKE_ASSERT(Pos <= size());
		constexpr size_type const n = std::min(Size, size() - Pos);
		return {m_impl.beg_ptr() + Pos, m_impl.beg_ptr() + Pos + n};
	}

	pointer data() { return m_impl.beg_ptr(); }
	const_pointer data() const { return m_impl.beg_ptr(); }

	pointer data_end() { return m_impl.end_ptr(); }
	const_pointer data_end() const { return m_impl.end_ptr(); }

	span trim_left(size_type n)
	{
		CORELIKE_ASSERT(n <= size());
		return span(m_impl.beg_ptr() + n, m_impl.end_ptr());
	}

	span<const_value_type, N> trim_left(size_type n) const
	{
		CORELIKE_ASSERT(n <= size());
		return span<const_value_type, N>(m_impl.beg_ptr() + n, m_impl.end_ptr());
	}

	span trim_right(size_type n)
	{
		CORELIKE_ASSERT(n <= size());
		return span(m_impl.beg_ptr(), m_impl.end_ptr() - n);
	}

	span<const_value_type, N> trim_right(size_type n) const
	{
		CORELIKE_ASSERT(n <= size());
		return span<const_value_type, N>(m_impl.beg_ptr(), m_impl.end_ptr() - n);
	}

	constexpr split_span<value_type, N> split();
	constexpr split_span<value_type, N> split_at(size_type n);
	constexpr split_span<value_type, N> split_with(const_iterator iter);

	constexpr split_span<const_value_type, N> split() const;
	constexpr split_span<const_value_type, N> split_at(size_type n) const;
	constexpr split_span<const_value_type, N> split_with(const_iterator iter) const;

	explicit operator bool() const { return false; } // to match split_span

	void clear() { m_impl.clear(); }

	void dump() const
	{
		std::cout << "beg: " << (void*)m_impl.beg_ptr() << '\n';
		std::cout << "end: " << (void*)m_impl.end_ptr() << '\n';
	}

private:
	detail::span_impl<T, N> m_impl;
};

template<typename T, size_type N = dynamic_extent>
using const_span = span<typename std::remove_const<T>::type const, N>;

template<typename T, size_type N = dynamic_extent>
using span_vector = std::vector<span<T, N>>;

template<typename T, size_type N = dynamic_extent>
using const_span_vector = std::vector<const_span<T, N>>;

template<typename T, size_type N = dynamic_extent>
const_span<byte, N> as_bytes(span<T, N> sp)
	{ return {static_cast<byte const*>(sp.data()), sp.size()}; }

template<typename T, size_type N = dynamic_extent, typename = typename std::enable_if<!std::is_const<T>::value>::type>
span<byte, N> as_writable_bytes(span<T, N> sp)
	{ return {static_cast<byte*>(sp.data()), sp.size()}; }

// [span.comparison], span comparison operators

template<typename T1, typename T2, size_type N1, size_type N2>
constexpr bool operator<(span<T1, N1> const& l, span<T2, N2> const& r)
	{ return std::lexicographical_compare(std::begin(l), std::end(l), std::begin(r), std::end(r)); }

template<typename T1, typename T2, size_type N1, size_type N2>
constexpr bool operator==(span<T1, N1> const& l, span<T2, N2> const& r)
	{ return std::equal(std::begin(l), std::end(l), std::begin(r), std::end(r)); }

template<typename T1, typename T2, size_type N1, size_type N2>
constexpr bool operator!=(span<T1, N1> const& l, span<T2, N2> const& r)
	{ return !(l == r); }

template<typename T1, typename T2, size_type N1, size_type N2>
constexpr bool operator<=(span<T1, N1> const& l, span<T2, N2> const& r)
	{ return l < r || l == r; }

template<typename T1, typename T2, size_type N1, size_type N2>
constexpr bool operator>(span<T1, N1> const& l, span<T2, N2> const& r)
	{ return !(l <= r); }

template<typename T1, typename T2, size_type N1, size_type N2>
constexpr bool operator>=(span<T1, N1> const& l, span<T2, N2> const& r)
	{ return l > r || l == r; }

/**
 * No default constructor. Currently these can only be
 * created from span<T, N>::split().
 */
template<typename T, size_type N = dynamic_extent>
class split_span
: public span<T, N>
{
public:
	using base = span<T, N>;
	using const_base = span<typename base::const_value_type, N>;

	using value_type             = T;
	using pointer                = typename base::pointer;
	using const_pointer          = typename base::const_pointer;

	using reference              = typename base::reference;
	using const_reference        = typename base::const_reference;

	using iterator               = typename base::iterator;
	using const_iterator         = typename base::const_iterator;

	using reverse_iterator       = typename base::reverse_iterator;
	using const_reverse_iterator = typename base::const_reverse_iterator;

	split_span(): base(), m_mid(nullptr) {}
	explicit split_span(base sp): base(sp), m_mid(std::begin(sp)) {}
	         split_span(split_span const& sp) = default;//: base(sp), m_mid(sp.m_mid) {}
	explicit split_span(pointer beg, pointer mid, pointer end): base(beg, end), m_mid(mid)
	{
		CORELIKE_ASSERT(beg <= mid);
		CORELIKE_ASSERT(mid <= end);
	}

	split_span& operator=(base sp) { reinterpret_cast<base&>(*this) = sp, m_mid = std::begin(sp); return *this; }
	split_span& operator=(split_span const& sp) = default;


	iterator mid()                       { return m_mid; }
	const_iterator mid()           const { return m_mid; }
	const_iterator cmid()          const { return m_mid; }
	reverse_iterator rmid()              { return reverse_iterator(m_mid); }
	const_reverse_iterator rmid()  const { return reverse_iterator(m_mid); }
	const_reverse_iterator crmid() const { return reverse_iterator(m_mid); }

	reference found() { CORELIKE_ASSERT(m_mid); CORELIKE_ASSERT(mid() != base::end()); return *mid(); }
	const_reference found() const { CORELIKE_ASSERT(m_mid); CORELIKE_ASSERT(mid() != base::end()); return *mid(); }

	explicit operator bool() const { return mid() != base::end(); }

	base first()              { return base(base::m_impl.beg_ptr(), m_mid); }
	base second()             { return base(m_mid, base::m_impl.end_ptr()); }

	const_base first()  const { return base(base::m_impl.beg_ptr(), m_mid); }
	const_base second() const { return base(m_mid, base::m_impl.end_ptr()); }

	base left()               { return first();  }
	base right()              { return second(); }

	const_base left()  const  { return first();  }
	const_base right() const  { return second(); }

	base before()             { return first();  }
	base after()              { return second(); }

	const_base before() const { return first();  }
	const_base after()  const { return second(); }

	void dump() const
	{
		base::dump();
		std::cout << "mid: " << (void*)m_mid << '\n';
	}

private:
	pointer m_mid = nullptr;
};

template<typename T, size_type N>
auto left(split_span<T, N> ssp) { return ssp.left(); }

template<typename T, size_type N>
auto right(split_span<T, N> ssp) { return ssp.right(); }

template<typename T, size_type N>
auto before(split_span<T, N> ssp) { return ssp.before(); }

template<typename T, size_type N>
auto after(split_span<T, N> ssp) { return ssp.after(); }

template<typename T, size_type N>
auto first(split_span<T, N> ssp) { return ssp.first(); }

template<typename T, size_type N>
auto second(split_span<T, N> ssp) { return ssp.second(); }


template<typename T, size_type N>
auto data(span<T, N> sp) { return sp.data(); }

template<typename T, size_type N>
auto data_end(span<T, N> sp) { return sp.data() + sp.size(); }


template<typename T, size_type N = dynamic_extent>
using const_split_span = split_span<typename std::remove_const<T>::type const, N>;

template<typename T, size_type N>
constexpr split_span<T, N> span<T, N>::split() { return split_at(0); }

template<typename T, size_type N>
constexpr split_span<T, N> span<T, N>::split_at(size_type n)
{
	CORELIKE_ASSERT(n <= N);
	return split_span<T, N>(m_impl.beg_ptr(), m_impl.beg_ptr() + n, m_impl.end_ptr());
}

template<typename T, size_type N>
constexpr split_span<T, N> span<T, N>::split_with(span<T, N>::const_iterator iter)
{
	CORELIKE_ASSERT(iter >= m_impl.beg_ptr());
	CORELIKE_ASSERT(iter <= m_impl.end_ptr());
	return split_span<T, N>(m_impl.beg_ptr(), iter, m_impl.end_ptr());
}

template<typename T, size_type N>
constexpr split_span<typename span<T, N>::const_value_type, N> span<T, N>::split() const
	{ return split_at(0); }

template<typename T, size_type N>
constexpr split_span<typename span<T, N>::const_value_type, N> span<T, N>::split_at(size_type n) const
{
	CORELIKE_ASSERT(n <= N);
	return split_span<typename span<T, N>::const_value_type, N>(m_impl.beg_ptr(), m_impl.beg_ptr() + n, m_impl.end_ptr());
}

template<typename T, size_type N>
constexpr split_span<typename span<T, N>::const_value_type, N> span<T, N>::split_with(span<T, N>::const_iterator iter) const
{
	CORELIKE_ASSERT(iter >= m_impl.beg_ptr());
	CORELIKE_ASSERT(iter <= m_impl.end_ptr());
	return split_span<typename span<T, N>::const_value_type, N>(m_impl.beg_ptr(), iter, m_impl.end_ptr());
}

} // namespace corelike
} // namespace galik

#endif // CORELIKE_SPAN_HPP
