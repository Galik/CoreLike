#ifndef CORE_LIKE_ASSERT_HPP
#define CORE_LIKE_ASSERT_HPP
//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <iostream>
#include <sstream>
#include <stdexcept>

#if !(defined(CORELIKE_THROW_ON_CONTRACT_VIOLATION) || defined(CORELIKE_TERMINATE_ON_CONTRACT_VIOLATION) ||  \
      defined(CORELIKE_UNENFORCED_ON_CONTRACT_VIOLATION))
#define CORELIKE_TERMINATE_ON_CONTRACT_VIOLATION
#endif

#if defined(CORELIKE_THROW_ON_CONTRACT_VIOLATION)
#define CORELIKE_NOEXCEPT
#ifdef NDEBUG
#define CORELIKE_TERMINATE_ACTION(msg) \
	do{ \
		std::ostringstream o; \
		o << msg << '\n'; \
		throw std::runtime_error(o.str()); \
	}while(0)
#else
#define CORELIKE_TERMINATE_ACTION(msg) \
	do{ \
		std::ostringstream o; \
		o << msg << ": " << __LINE__ << ": " << __FILE__ << '\n'; \
		throw std::runtime_error(o.str()); \
	}while(0)
#endif
#elif defined(CORELIKE_TERMINATE_ON_CONTRACT_VIOLATION)
#define CORELIKE_NOEXCEPT noexcept
#ifdef NDEBUG
#define CORELIKE_TERMINATE_ACTION(msg) \
	do{ \
		std::cerr << msg << '\n'; \
		std::terminate(); \
	}while(0)
#else
#define CORELIKE_TERMINATE_ACTION(msg) \
	do{ \
		std::cerr << msg << ": " << __LINE__ << ": " << __FILE__ << '\n'; \
		std::terminate(); \
	}while(0)
#endif
#elif defined(CORELIKE_UNENFORCED_ON_CONTRACT_VIOLATION)
#define CORELIKE_NOEXCEPT noexcept
#define CORELIKE_TERMINATE_ACTION(msg) do{}while(0)
#endif

#define CORELIKE_ASSERT(test) do{ if(!(test)) CORELIKE_TERMINATE_ACTION("CoreLike Assert failure: " << #test); }while(0)
#define CORELIKE_ASSERT_MSG(test,msg) do{ if(!(test)) CORELIKE_TERMINATE_ACTION(#test << ": " << msg); }while(0)

#endif // CORE_LIKE_ASSERT_HPP
