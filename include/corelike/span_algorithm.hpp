#ifndef CORELIKE_SPAN_ALGORITHM_HPP
#define CORELIKE_SPAN_ALGORITHM_HPP
//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <algorithm>
#include <iterator>

#include "span.hpp"
#include "assert.hpp"

namespace galik {
namespace corelike {

//template<template<class> class Span, class T>
//auto mid(Span<T> sp) { return sp.mid(); }

template<typename T, size_type N> inline
auto mid(split_span<T, N> ssp) { return ssp.mid(); }

// Algorithms

// Non-modifying sequence operations

template<typename T, size_type N, typename UnaryPredicate> inline
auto all_of(span<T, N> sp, UnaryPredicate p)
	{ return std::all_of(std::begin(sp), std::end(sp), p); }

template<typename T, size_type N, typename UnaryPredicate> inline
auto any_of(span<T, N> sp, UnaryPredicate p)
	{ return std::any_of(std::begin(sp), std::end(sp), p); }

template<typename T, size_type N, typename UnaryPredicate> inline
auto none_of(span<T, N> sp, UnaryPredicate p)
	{ return std::none_of(std::begin(sp), std::end(sp), p); }


template<typename T, size_type N, typename UnaryFunction> inline
auto for_each(span<T, N> sp, UnaryFunction f)
	{ return std::for_each(std::begin(sp), std::end(sp), f); }


template<typename T, size_type N> inline
auto count(span<T, N> sp, T const& v)
	{ return std::count(std::begin(sp), std::end(sp), v); }

template<typename T, size_type N, typename UnaryPredicate> inline
auto count_if(span<T, N> sp, UnaryPredicate p)
	{ return std::count_if(std::begin(sp), std::end(sp), p); }


template<typename T1, size_type N1, typename T2, size_type N2>
constexpr std::pair<split_span<T1, N1>, split_span<T2, N2>> mismatch(span<T1, N1> sp1, span<T2, N2> sp2)
{
	auto mm = std::mismatch(std::begin(sp1), std::end(sp1), std::begin(sp2), std::end(sp2));
	auto ssp1 = split_span<T1, N1>(std::begin(sp1), mm.first, std::end(sp1));
	auto ssp2 = split_span<T2, N2>(std::begin(sp2), mm.second, std::end(sp2));
	return std::make_pair(ssp1, ssp2);
}

template<typename T1, size_type N1, typename T2, size_type N2, typename BinaryPredicate>
constexpr std::pair<split_span<T1, N1>, split_span<T2, N2>> mismatch(span<T1, N1> sp1, span<T2, N2> sp2, BinaryPredicate p)
{
	auto mm = std::mismatch(std::begin(sp1), std::end(sp1), std::begin(sp2), std::end(sp2), p);
	auto ssp1 = split_span<T1, N1>(std::begin(sp1), mm.first, std::end(sp1));
	auto ssp2 = split_span<T2, N2>(std::begin(sp2), mm.second, std::end(sp2));
	return std::make_pair(ssp1, ssp2);
}

template<typename T, size_type N> inline
split_span<T, N> find(span<T, N> sp, typename std::remove_const<T>::type const& v)
{
	auto found = std::find(std::begin(sp), std::end(sp), v);
	return split_span<T, N>(std::begin(sp), found, std::end(sp));
}

template<typename T, size_type N, typename UnaryPredicate> inline
split_span<T, N> find_if(span<T, N> sp, UnaryPredicate p)
{
	auto found = std::find_if(std::begin(sp), std::end(sp), p);
	return split_span<T, N>(std::begin(sp), found, std::end(sp));
}

template<typename T, size_type N, typename UnaryPredicate> inline
split_span<T, N> find_if_not(span<T, N> sp, UnaryPredicate p)
{
	auto found = std::find_if_not(std::begin(sp), std::end(sp), p);
	return split_span<T, N>(std::begin(sp), found, std::end(sp));
}

template<typename T1, size_type N1, typename T2, size_type N2> inline
split_span<T1, N1> find_end(span<T1, N1> sp, span<T2, N2> ss)
{
	auto found = std::find_end(std::begin(sp), std::end(sp), std::begin(ss), std::end(ss));
	return split_span<T1, N1>(std::begin(sp), found, std::end(sp));
}

template<typename T1, size_type N1, typename T2, size_type N2, typename BinaryPredicate> inline
split_span<T1, N1> find_end(span<T1, N1> sp, span<T2, N2> ss, BinaryPredicate p)
{
	auto found = std::find_end(std::begin(sp), std::end(sp), std::begin(ss), std::end(ss), p);
	return split_span<T1, N1>(std::begin(sp), found, std::end(sp));
}

template<typename T1, size_type N1, typename T2, size_type N2> inline
split_span<T1, N1> find_first_of(span<T1, N1> sp, span<T2, N2> ss)
{
	auto found = std::find_first_of(std::begin(sp), std::end(sp), std::begin(ss), std::end(ss));
	return split_span<T1, N1>(std::begin(sp), found, std::end(sp));
}

template<typename T1, size_type N1, typename T2, size_type N2, typename BinaryPredicate> inline
split_span<T1, N1> find_first_of(span<T1, N1> sp, span<T2, N2> ss, BinaryPredicate p)
{
	auto found = std::find_first_of(std::begin(sp), std::end(sp), std::begin(ss), std::end(ss), p);
	return split_span<T1, N1>(std::begin(sp), found, std::end(sp));
}

template<typename T, size_type N> inline
split_span<T, N> adjacent_find(span<T, N> sp)
{
	auto found = std::adjacent_find(std::begin(sp), std::end(sp));
	return split_span<T, N>(std::begin(sp), found, std::end(sp));
}

template<typename T, size_type N, typename BinaryPredicate> inline
split_span<T, N> adjacent_find(span<T, N> sp, BinaryPredicate p)
{
	auto found = std::adjacent_find(std::begin(sp), std::end(sp), p);
	return split_span<T, N>(std::begin(sp), found, std::end(sp));
}

template<typename T1, size_type N1, typename T2, size_type N2> inline
split_span<T1, N1> search(span<T1, N1> sp, span<T2, N2> ss)
{
	auto found = std::search(std::begin(sp), std::end(sp), std::begin(ss), std::end(ss));
	return split_span<T1, N1>(std::begin(sp), found, std::end(sp));
}

template<typename T1, size_type N1, typename T2, size_type N2, typename BinaryPredicate> inline
split_span<T1, N1> search(span<T1, N1> sp, span<T2, N2> ss, BinaryPredicate p)
{
	auto found = std::search(std::begin(sp), std::end(sp), std::begin(ss), std::end(ss), p);
	return split_span<T1, N1>(std::begin(sp), found, std::end(sp));
}

template<typename T, size_type N> inline
split_span<T, N> search_n(span<T, N> sp, std::size_t count, T const& value)
{
	auto found = std::search_n(std::begin(sp), std::end(sp), count, value);
	return split_span<T, N>(std::begin(sp), found, std::end(sp));
}

template<typename T, size_type N, typename BinaryPredicate> inline
split_span<T, N> search_n(span<T, N> sp, std::size_t count, BinaryPredicate p)
{
	auto found = std::search_n(std::begin(sp), std::end(sp), count, p);
	return split_span<T, N>(std::begin(sp), found, std::end(sp));
}


// Modifying sequence operations


// copy/copy_if - moved to content_span


template<typename T1, typename T2, size_type N1, size_type N2>
constexpr split_span<T2, N2> copy(span<T1, N1> sp, span<T2, N2> out)
{
	auto end_sp = out.size() < sp.size() ? std::begin(sp) + out.size() : std::end(sp);
	return split_span<T2, N2>(std::begin(out), std::copy(std::begin(sp), end_sp, std::begin(out)), std::end(out));
}

template<typename T1, typename T2, size_type N1, size_type N2, typename UnaryPredicate>
constexpr split_span<T2, N2> copy_if(span<T1, N1> sp, span<T2, N2> out, UnaryPredicate p)
{
	auto end_sp = out.size() < sp.size() ? std::begin(sp) + out.size() : std::end(sp);
	return split_span<T2, N2>(std::begin(out), std::copy_if(std::begin(sp), end_sp, std::begin(out), p), std::end(out));
}

template<typename T1, typename T2, size_type N1, size_type N2>
constexpr split_span<T2, N2> copy_backward(span<T1, N1> sp, span<T2, N2> out)
{
	auto end_sp = out.size() < sp.size() ? std::begin(sp) + out.size() : std::end(sp);
	return split_span<T2, N2>(std::begin(out), std::copy_backward(std::begin(sp), end_sp, std::begin(out)), std::end(out));
}

template<typename T1, typename T2, size_type N1, size_type N2>
constexpr split_span<T2, N2> move(span<T1, N1> sp, span<T2, N2> out)
{
	auto end_sp = out.size() < sp.size() ? std::begin(sp) + out.size() : std::end(sp);
	return split_span<T2, N2>(std::begin(out), std::move(std::begin(sp), end_sp, std::begin(out)), std::end(out));
}

template<typename T1, typename T2, size_type N1, size_type N2>
constexpr split_span<T2, N2> move_backward(span<T1, N1> sp, span<T2, N2> out)
{
	auto end_sp = out.size() < sp.size() ? std::begin(sp) + out.size() : std::end(sp);
	return split_span<T2, N2>(std::begin(out), std::move_backward(std::begin(sp), end_sp, std::begin(out)), std::end(out));
}

//template<typename T, size_type N, typename OutIter>
//constexpr span<T, N> copy(span<T, N> sp, OutIter out)
//	{ return span<T, N>(&(*out), std::copy(std::begin(sp), std::end(sp), out)); }
//
//template<typename T, size_type N, typename OutIter, typename UnaryPredicate>
//constexpr span<T, N> copy_if(span<T, N> sp, OutIter out, UnaryPredicate p)
//	{ return span<T, N>(&(*out), &*std::copy_if(std::begin(sp), std::end(sp), out, p)); }
//
//template<typename T, size_type N, typename OutIter>
//constexpr span<T, N> copy_backward(span<T, N> sp, OutIter out)
//{ return span<T, N>(&(*out), std::copy_backward(std::begin(sp), std::end(sp), out)); }
//
//template<typename T, size_type N, typename OutIter>
//constexpr span<T, N> move(span<T, N> sp, OutIter out)
//{ return span<T, N>(&(*out), std::move(std::begin(sp), std::end(sp), out)); }
//
//template<typename T, size_type N, typename OutIter>
//constexpr span<T, N> move_backward(span<T, N> sp, OutIter out)
//{ return span<T, N>(&(*out), std::move_backward(std::begin(sp), std::end(sp), out)); }


template<typename T, size_type N> inline
void fill(span<T, N> sp, T const& v)
	{ std::fill(std::begin(sp), std::end(sp), v); }


template<typename T1, size_type N1, typename T2, size_type N2, typename UnaryOperation> inline
auto transform(span<T1, N1> sp, span<T2, N2> out, UnaryOperation op)
{
	std::size_t n = std::min(sp.size(), out.size());
	std::transform(std::begin(sp), std::begin(sp) + n, std::begin(out), op);
	return split_span<T2, N2>(std::begin(out), std::begin(out) + n, std::end(out));
}

// Transform common indices between `sp1`, `sp2` and `out` returning
// a `split_span` of the actual output.
template<typename T1, size_type N1, typename T2, size_type N2, typename T3, size_type N3, typename BinaryOperation> inline
auto transform(span<T1, N1> sp1, span<T2, N2> sp2, span<T3, N3> out, BinaryOperation op)
{
	std::size_t n = std::min(std::min(sp1.size(), sp2.size()), out.size());
	std::transform(std::begin(sp1), std::begin(sp1) + n, std::begin(sp2), std::begin(out), op);
	return split_span<T3, N3>(std::begin(out), std::begin(out) + n, std::end(out));
}

template<typename T, size_type N, typename Generator>
constexpr void generate(span<T, N> sp, Generator g)
	{ std::generate(std::begin(sp), std::end(sp), g); }


/**
 * Remove value from span returning span containing
 * only valid values.
 *
 * NOTE: Does this even make sense for a span?
 *       What about the indeterminate values at the end of the container?
 *
 * @param sp
 * @param value
 * @return
 */
template<typename T, size_type N> inline
constexpr split_span<T, N> remove(span<T, N> sp, T const& value)
	{ return split_span<T, N>(std::begin(sp), std::remove(std::begin(sp), std::end(sp), value), std::end(sp)); }

template<typename T, size_type N, typename UnaryPredicate> inline
constexpr split_span<T, N> remove_if(span<T, N> sp, UnaryPredicate p)
	{ return split_span<T, N>(std::begin(sp), std::remove_if(std::begin(sp), std::end(sp), p), std::end(sp)); }

template<typename T, size_type N> inline
constexpr void reverse(span<T, N> sp)
	{ std::reverse(std::begin(sp), std::end(sp)); }

//template<typename T, size_type N, typename OutIter> inline
//constexpr span<T, N> reverse_copy(span<T, N> sp, OutIter out)
//	{ return span<T, N>(&(*out), std::reverse_copy(std::begin(sp), std::end(sp), out)); }

template<typename T, size_type N> inline
constexpr split_span<T, N> rotate(split_span<T, N> sp)
	{ return split_span<T, N>(std::begin(sp), std::rotate(std::begin(sp), sp.mid(), std::end(sp)), std::end(sp)); }

//template<typename T, size_type N, typename OutIter> inline
//constexpr span<T, N> rotate_copy(split_span<T, N> sp, OutIter out)
//	{ return span<T, N>(&(*out), std::rotate_copy(std::begin(sp), sp.mid(), std::end(sp), out)); }


// TODO: set for C++20
//template<typename T>
//constexpr auto shift_left(span<T, N> sp, typename span<T, N>::difference_type n)
//	{ return std::shift_left(std::begin(sp), std::end(sp), n); }
//
//template<typename T>
//constexpr auto shift_right(span<T, N> sp, typename span<T, N>::difference_type n)
//	{ return std::shift_right(std::begin(sp), std::end(sp), n); }

template<typename T, size_type N, typename URBG> inline
void shuffle(span<T, N> sp, URBG&& g)
	{ std::shuffle(std::begin(sp), std::end(sp), std::forward<URBG>(g)); }

// TODO: C++17
//template<typename T, typename OutIter, typename Distance, typename URBG> inline
//auto sample(span<T, N> sp, OutIter out, Distance n, URBG&& g)
//	{ return std::sample(std::begin(sp), std::end(sp), n, std::forward<URBG>(g)); }



template<typename T, size_type N> inline
constexpr split_span<T, N> unique(span<T, N> sp)
	{ return split_span<T, N>(std::begin(sp), std::unique(std::begin(sp), std::end(sp)), std::end(sp)); }

template<typename T, size_type N, typename BinaryPredicate> inline
constexpr split_span<T, N> unique(span<T, N> sp, BinaryPredicate p)
	{ return split_span<T, N>(std::begin(sp), std::unique(std::begin(sp), std::end(sp), p), std::end(sp)); }


//template<typename T, size_type N, typename OutIter> inline
//constexpr span<T, N> unique_copy(span<T, N> sp, OutIter out)
//	{ return span<T, N>(&(*out), std::unique_copy(std::begin(sp), std::end(sp), out)); }
//
//template<typename T, size_type N, typename OutIter, typename BinaryPredicate> inline
//constexpr span<T, N> unique_copy(span<T, N> sp, OutIter out, BinaryPredicate p)
//	{ return span<T, N>(&(*out), std::unique_copy(std::begin(sp), std::end(sp), out, p)); }

/// Partitioning operations

template<typename T, size_type N, typename UnaryPredicate> inline
constexpr bool is_partitioned(span<T, N> sp, UnaryPredicate p)
	{ return std::is_partitioned(std::begin(sp), std::end(sp), p); }

template<typename T, size_type N, typename UnaryPredicate> inline
constexpr split_span<T, N> partition(span<T, N> sp, UnaryPredicate p)
	{ return split_span<T, N>(std::begin(sp), std::partition(std::begin(sp), std::end(sp), p), std::end(sp)); }

//template<typename T, size_type N, typename OutIterTrue, typename OutIterFalse, typename UnaryPredicate>
//constexpr std::pair<span<T>, span<T>> partition_copy(span<T, N> sp,
//	OutIterTrue t, OutIterFalse f, UnaryPredicate p)
//{
//	auto pp = std::partition_copy(std::begin(sp), std::end(sp), t, f, p);
//	return std::make_pair(span<T>(t, pp.first), span<T>(f, pp.second));
//}

template<typename T, size_type N, typename UnaryPredicate> inline
constexpr split_span<T, N> stable_partition(span<T, N> sp, UnaryPredicate p)
	{ return split_span<T, N>(std::begin(sp), std::stable_partition(std::begin(sp), std::end(sp), p), std::end(sp)); }

template<typename T, size_type N, typename UnaryPredicate> inline
constexpr split_span<T, N> partition_point(span<T, N> sp, UnaryPredicate p)
	{ return split_span<T, N>(std::begin(sp), std::partition_point(std::begin(sp), std::end(sp), p), std::end(sp)); }

/// Sorting operations

template<typename T, size_type N> inline
constexpr bool is_sorted(span<T, N> sp)
	{ return std::is_sorted(std::begin(sp), std::end(sp)); }

template<typename T, size_type N, typename Compare> inline
constexpr bool is_sorted(span<T, N> sp, Compare cmp)
	{ return std::is_sorted(std::begin(sp), std::end(sp), cmp); }

template<typename T, size_type N> inline
constexpr split_span<T, N> is_sorted_until(span<T, N> sp)
	{ return split_span<T, N>(std::begin(sp), std::is_sorted_until(std::begin(sp), std::end(sp)), std::end(sp)); }

template<typename T, size_type N, typename Compare> inline
constexpr split_span<T, N> is_sorted_until(span<T, N> sp, Compare cmp)
	{ return split_span<T, N>(std::begin(sp), std::is_sorted_until(std::begin(sp), std::end(sp), cmp), std::end(sp)); }

template<typename T, size_type N> inline
constexpr void sort(span<T, N> sp)
	{ std::sort(std::begin(sp), std::end(sp)); }

template<typename T, size_type N, typename Compare> inline
constexpr void sort(span<T, N> sp, Compare cmp)
	{ std::sort(std::begin(sp), std::end(sp), cmp); }

template<typename T, size_type N> inline
constexpr void partial_sort(split_span<T, N> ssp)
	{ std::partial_sort(std::begin(ssp), ssp.mid(), std::end(ssp)); }

template<typename T, size_type N, typename Compare> inline
constexpr void partial_sort(split_span<T, N> ssp, Compare cmp)
	{ std::partial_sort(std::begin(ssp), ssp.mid(), std::end(ssp), cmp); }

template<typename T, size_type N> inline
constexpr void stable_sort(span<T, N> sp)
	{ std::stable_sort(std::begin(sp), std::end(sp)); }

template<typename T, size_type N, typename Compare> inline
constexpr void stable_sort(span<T, N> sp, Compare cmp)
	{ std::stable_sort(std::begin(sp), std::end(sp), cmp); }

/// Binary search operations (on sorted ranges)

template<typename T, size_type N>
constexpr split_span<T, N> lower_bound(span<T, N> sp, T const& v)
	{ return split_span<T, N>(std::begin(sp), std::lower_bound(std::begin(sp), std::end(sp), v), std::end(sp)); }

template<typename T, size_type N, typename Compare>
constexpr split_span<T, N> lower_bound(span<T, N> sp, T const& v, Compare cmp)
	{ return split_span<T, N>(std::begin(sp), std::lower_bound(std::begin(sp), std::end(sp), v, cmp), std::end(sp)); }

template<typename T, size_type N>
constexpr split_span<T, N> upper_bound(span<T, N> sp, T const& v)
	{ return split_span<T, N>(std::begin(sp), std::upper_bound(std::begin(sp), std::end(sp), v), std::end(sp)); }

template<typename T, size_type N, typename Compare>
constexpr split_span<T, N> upper_bound(span<T, N> sp, T const& v, Compare cmp)
	{ return split_span<T, N>(std::begin(sp), std::upper_bound(std::begin(sp), std::end(sp), v, cmp), std::end(sp)); }

template<typename T, size_type N>
constexpr bool binary_search(span<T, N> sp, T const& v)
	{ return std::binary_search(std::begin(sp), std::end(sp), v); }

template<typename T, size_type N, typename Compare>
constexpr bool binary_search(span<T, N> sp, T const& v, Compare cmp)
	{ return std::binary_search(std::begin(sp), std::end(sp), v, cmp); }

template<typename T, size_type N>
constexpr span<T> equal_range(span<T, N> sp, T const& v)
{
	auto pp = std::equal_range(std::begin(sp), std::end(sp), v);
	return span<T>(pp.first, pp.second);
}

template<typename T, size_type N, typename Compare>
constexpr span<T> equal_range(span<T, N> sp, T const& v, Compare cmp)
{
	auto pp = std::equal_range(std::begin(sp), std::end(sp), v, cmp);
	return span<T>(pp.first, pp.second);
}

/// Set operations (on sorted ranges)

/// Heap operations

/// Minimum/maximum operations

template<typename T, size_type N>
constexpr split_span<T, N> min_element(span<T, N> sp)
	{ return split_span<T, N>(std::begin(sp), std::min_element(std::begin(sp), std::end(sp)), std::end(sp)); }

template<typename T, size_type N, typename Compare>
constexpr split_span<T, N> min_element(span<T, N> sp, Compare cmp)
	{ return split_span<T, N>(std::begin(sp), std::min_element(std::begin(sp), std::end(sp), cmp), std::end(sp)); }


/// Comparison operations


/// Permutation operations

template<typename T, size_type N>
bool next_permutation(span<T, N> sp)
	{ return std::next_permutation(std::begin(sp), std::end(sp)); }

template<typename T, size_type N, typename Compare>
bool next_permutation(span<T, N> sp, Compare cmp)
	{ return std::next_permutation(std::begin(sp), std::end(sp), cmp); }

// Non standard
template<typename T, size_type N, typename Func>
void for_each_permutation(span<T, N> sp, Func func)
{
	do func(sp);
	while(next_permutation(sp));
}

// Non standard
template<typename T, size_type N, typename Compare, typename Func>
void for_each_permutation(span<T, N> sp, Compare cmp, Func func)
{
	do func(sp);
	while(next_permutation(sp, cmp));
}


// Other

template<typename T, size_type N1, typename U, size_type N2> inline
bool is_a_subspan_of(span<T, N1> set, span<U, N2> sub)
{
	return std::begin(sub) >= std::begin(set) && std::end(sub) <= std::end(set);
}

/**
 * Make a subspan with the same offset and size as the
 * supplied subspan for the given span.
 * @param sp The span to obtain a subspan from.
 * @param set The given span.
 * @param sub The supplied subspan.
 * @return A subspan of `sp`.
 */
template<typename T, size_type N1, typename U, size_type N2> inline
span<T, N1> make_subspan_like(span<T, N1> sp, span<U, N2> set, span<U, N2> sub)
{
	CORELIKE_ASSERT(is_a_subspan_of(set, sub));

	auto off = std::begin(sub) - std::begin(set);

	CORELIKE_ASSERT(off <= sp.size());
	CORELIKE_ASSERT(off + sub.size() <= sp.size());

	return {std::begin(sp) + off, std::begin(sp) + off + sub.size()};
}

template<typename T, size_type N> inline
span_vector<T, N> divide_up(span<T, N> sp, std::size_t n)
{
	if(!n || sp.empty())
		return {};

	span_vector<T, N> v;
	v.reserve(n);

	auto div = sp.size() / n;
	auto rem = sp.size() % n;

	auto p = sp.data();

	for(std::size_t i = 0; i < n; ++i)
	{
		if(rem)
		{
			v.emplace_back(p, p + div + 1);
			p = p + div + 1;
			--rem;
			continue;
		}

		if(div)
		{
			v.emplace_back(p, p + div);
			p = p + div;
		}
	}

	return v;
}

// Trimming

template<typename T1, size_type N1, typename T2, size_type N2> inline
auto trim_left(span<T1, N1> sp, span<T2, N2> t)
{
	span<T1> r = sp;
	while(!r.empty() && find(t, r[0])) { r = r.subspan(1); }
	return r;
}

template<typename T1, size_type N1, typename T2, size_type N2> inline
auto trim_right(span<T1, N1> sp, span<T2, N2> t)
{
	span<T1> r = sp;
	while(!r.empty() && find(t, r[r.size() - 1])) { r = r.subspan(0, r.size() - 1); }
	return r;
}

template<typename T1, size_type N1, typename T2, size_type N2> inline
auto trim(span<T1, N1> sp, span<T2, N2> t)
{
	return trim_right(trim_left(sp, t), t);
}

template<typename T, size_type N> inline
auto trim_left(span<T, N> sp, typename std::remove_const<T>::type const& t)
{
	span<T> r = sp;
	while(!r.empty() && r[0] == t) { r = r.subspan(1); }
	return r;
}

template<typename T, size_type N> inline
auto trim_right(span<T, N> sp, typename std::remove_const<T>::type const& t)
{
	span<T> r = sp;
	while(!r.empty() && r[r.size() - 1] == t) { r = r.subspan(0, r.size() - 1); }
	return r;
}

template<typename T, size_type N> inline
auto trim(span<T, N> sp, typename std::remove_const<T>::type const& t)
{
	return trim_right(trim_left(sp, t), t);
}

} // namespace corelike
} // namespace galik

#endif // CORELIKE_SPAN_ALGORITHM_HPP
