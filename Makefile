
-include Makefile.local

VERSION := v0.0.0

PREFIX ?= /usr/local
PC_INSTALL_PATH ?= $(shell pkg-config --variable pc_path pkg-config)
PC_INSTALL_DIR = $(lastword $(subst :, ,$(PC_INSTALL_PATH)))

MD := mkdir -p
INSTALL := install

SRC := tests
BIN := bin
TOOL_SRC := tools

CXX := g++

CXXFLAGS := -std=c++14 -MMD -MP -pedantic-errors -Wall -Wextra \
	-DCORELIKE_THROW_ON_CONTRACT_VIOLATION

CPPFLAGS := -Iinclude

TEST_SRCS := $(wildcard $(SRC)/*.cpp)
TEST_PRGS := $(patsubst $(SRC)/%.cpp,$(BIN)/%,$(TEST_SRCS))
TEST_DEPS := $(patsubst $(SRC)/%.cpp,$(BIN)/%.d,$(TEST_SRCS))

TOOL_SRCS := $(wildcard $(TOOL_SRC)/*.cpp)
TOOL_PRGS := $(patsubst $(TOOL_SRC)/%.cpp,$(BIN)/%,$(TOOL_SRCS))
TOOL_DEPS := $(patsubst $(TOOL_SRC)/%.cpp,$(BIN)/%.d,$(TOOL_SRCS))

PC_MAP := "PREFIX=$(PREFIX)" "VERSION=$(VERSION)" "INCLUDE=$(PREFIX)/include"

all: prereqs $(TOOL_PRGS) $(TEST_PRGS) corelike.pc

$(BIN)/%:$(SRC)/%.cpp
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -o $@ $<

$(BIN)/%:$(TOOL_SRC)/%.cpp
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -o $@ $<
	
corelike.pc: corelike.pc.in $(BIN)/replace-text
	@$(BIN)/replace-text $(PC_MAP) < "$<" > "$@"
	@pkg-config  --validate "$@"

install: corelike.pc
	$(INSTALL) -d $(PREFIX)/include/corelike
	$(INSTALL) include/corelike/*.hpp $(PREFIX)/include/corelike
	$(INSTALL) -d $(PC_INSTALL_DIR)
	$(INSTALL) corelike.pc $(PC_INSTALL_DIR)	

run-tests: $(TEST_PRGS)
	@./run-tests.sh

-include $(TEST_DEPS)

show:
	@echo PC_INSTALL_DIR: $(PC_INSTALL_DIR)

clean-pc:
	$(RM) corelike.pc

clean:
	$(RM) $(TEST_PRGS) $(TOOL_PRGS) corelike.pc

prereqs:
	$(MD) bin

.PHONY: prereqs