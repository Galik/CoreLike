/*
 * replace-text.cpp
 *
 *  Created on: 27 Aug 2018
 *      Author: galik
 */

#include <cstdlib>
#include <map>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>
#include <iostream>

std::string& replace_all_mute(std::string& s, std::string const& from, std::string const& to)
{
	if(!from.empty())
		for(std::size_t pos = 0; (pos = s.find(from, pos) + 1); pos += to.size())
			s.replace(--pos, from.size(), to);
	return s;
}

std::string replace_all_copy(std::string s, std::string const& from, std::string const& to)
	{ return replace_all_mute(s, from, to); }

// usage: replace-text "KEY1=VALUE1" "KEY2=VALUE2" ... etc.

int main(int, char** argv) try
{
	std::istream& is = std::cin;
	std::ostream& os = std::cout;

	std::map<std::string, std::string> subs;

	{
		std::string from;
		std::string to;
		for(auto arg = argv + 1; *arg; ++arg)
		{
			std::istringstream iss(*arg);
			if(std::getline(std::getline(iss, from, '='),to))
				subs[from] = to;
		}
	}

	std::string text;

	{
		for(char c; is.get(c);)
			text += c;

		for(auto const& sub: subs)
			replace_all_mute(text, sub.first, sub.second);
	}

	os << text;

	return EXIT_SUCCESS;
}
catch(std::exception const& e)
{
	std::cerr << e.what() << '\n';
	return EXIT_FAILURE;
}
catch(...)
{
	std::cerr << "unknown error" << '\n';
	return EXIT_FAILURE;
}
