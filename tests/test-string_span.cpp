//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <vector>
#include "../include/corelike/string_span.hpp"

namespace cl { using namespace galik::corelike; }

TEST_CASE("Framework")
{
	SECTION("creating split_span")
	{

	}
}


TEST_CASE("Comparators")
{
	SECTION("span vs span")
	{
		std::string s1 = "test";
		std::string s2 = "test";
		std::string s3 = "blip";

		auto sp1 = cl::make_string_span(s1);
		auto sp2 = cl::make_string_span(s2);
		auto sp3 = cl::make_string_span(s3);

		REQUIRE(sp1 == sp2);
		REQUIRE(sp2 == sp1);
		REQUIRE_FALSE(sp1 == sp3);
		REQUIRE_FALSE(sp3 == sp1);
		REQUIRE_FALSE(sp2 == sp3);
		REQUIRE_FALSE(sp3 == sp2);

		REQUIRE_FALSE(sp1 != sp2);
		REQUIRE_FALSE(sp2 != sp1);
		REQUIRE(sp1 != sp3);
		REQUIRE(sp3 != sp1);
		REQUIRE(sp2 != sp3);
		REQUIRE(sp3 != sp2);

		REQUIRE_FALSE(sp1 < sp2);
		REQUIRE_FALSE(sp2 < sp1);
		REQUIRE_FALSE(sp1 < sp3);
		REQUIRE(sp3 < sp1);
		REQUIRE_FALSE(sp2 < sp3);
		REQUIRE(sp3 < sp2);

		REQUIRE_FALSE(sp1 > sp2);
		REQUIRE_FALSE(sp2 > sp1);
		REQUIRE(sp1 > sp3);
		REQUIRE_FALSE(sp3 > sp1);
		REQUIRE(sp2 > sp3);
		REQUIRE_FALSE(sp3 > sp2);

		REQUIRE(sp1 <= sp2);
		REQUIRE(sp2 <= sp1);
		REQUIRE_FALSE(sp1 <= sp3);
		REQUIRE(sp3 <= sp1);
		REQUIRE_FALSE(sp2 <= sp3);
		REQUIRE(sp3 <= sp2);

		REQUIRE(sp1 >= sp2);
		REQUIRE(sp2 >= sp1);
		REQUIRE(sp1 >= sp3);
		REQUIRE_FALSE(sp3 >= sp1);
		REQUIRE(sp2 >= sp3);
		REQUIRE_FALSE(sp3 >= sp2);
	}

	SECTION("span vs Chart(&)[N]")
	{
		std::string s = "test";
//		std::string s2 = "test";
//		std::string s3 = "blip";

		auto sp = cl::make_string_span(s);
//		auto sp2 = cl::make_string_span(s2);
//		auto sp3 = cl::make_string_span(s3);

		REQUIRE(sp == "test");
		REQUIRE("test" == sp);
		REQUIRE_FALSE(sp == "blip");
		REQUIRE_FALSE("blip" == sp);

		REQUIRE_FALSE(sp != "test");
		REQUIRE_FALSE("test" != sp);
		REQUIRE(sp != "blip");
		REQUIRE("blip" != sp);

		REQUIRE_FALSE(sp < "test");
//		REQUIRE_FALSE("test" < sp);
		REQUIRE_FALSE(sp < "blip");
		REQUIRE("blip" < sp);

		REQUIRE_FALSE(sp > "test");
//		REQUIRE_FALSE("test" > sp);
		REQUIRE(sp > "blip");
		REQUIRE_FALSE("blip" > sp);

		REQUIRE(sp <= "test");
//		REQUIRE("test" <= sp);
		REQUIRE_FALSE(sp <= "blip");
		REQUIRE("blip" <= sp);

		REQUIRE(sp >= "test");
//		REQUIRE("test" >= sp);
		REQUIRE(sp >= "blip");
		REQUIRE_FALSE("blip" >= sp);
	}
}

