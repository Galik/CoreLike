#ifndef CORELIKE_TEST_HPP
#define CORELIKE_TEST_HPP
//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include <random>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

namespace galik {
namespace corelike {

inline
auto& random_engine()
{
	thread_local static std::mt19937 mt{std::random_device{}()};
	return mt;
}

template<typename Numeric> inline
Numeric random_number(Numeric min, Numeric max)
{
	using dist_type = typename std::conditional<std::is_integral<Numeric>::value,
		std::uniform_int_distribution<Numeric>,
		std::uniform_real_distribution<Numeric>
	>::type;

	return dist_type(min, max)(random_engine());
}

} // namespace corelike
} // namespace galik



#endif // CORELIKE_TEST_HPP
