//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include "test.hpp"

#include <iostream>

#include "../include/corelike/content_span.hpp"
#include "../include/corelike/span_algorithm.hpp"

namespace cl { using namespace galik::corelike; }

TEST_CASE("dynamic_extent constructors")
{
	SECTION("default")
	{
		cl::content_span<int> csp;

		REQUIRE(csp.size() == 0);
	}

	SECTION("Iter begin, Iter end")
	{
		int arr[] = {1, 2, 3, 4, 5, 6};
		cl::content_span<int> csp(std::cbegin(arr), std::cend(arr));

		REQUIRE(csp.size() == std::distance(std::cbegin(arr), std::cend(arr)));
		REQUIRE(std::equal(std::cbegin(csp), std::cend(csp), std::cbegin(arr)));
	}

	SECTION("span<T> sp")
	{
		int arr[] = {1, 2, 3, 4, 5, 6};
		cl::span<int> sp(arr);
		cl::content_span<int> csp(sp);

		REQUIRE(csp.size() == sizeof(arr)/sizeof(*arr));
		REQUIRE(std::equal(std::cbegin(csp), std::cend(csp), std::cbegin(arr)));
	}

	SECTION("size_type n")
	{
		cl::content_span<int> csp(3);

		REQUIRE(csp.size() == 3);
		REQUIRE(std::all_of(std::cbegin(csp), std::cend(csp), [](int i){ return i == 0; }));
	}

	SECTION("std::vector<T>&& v")
	{
		std::vector<int> v = {1, 2, 3, 4, 5, 6};
		auto const cv = v;
		cl::content_span<int> csp(std::move(v));

		REQUIRE(csp.size() == cv.size());
		REQUIRE(std::equal(std::cbegin(csp), std::cend(csp), std::cbegin(cv)));
	}
}

TEST_CASE("static_extent constructors")
{
	SECTION("default")
	{
		cl::content_span<int, 3> csp;

		REQUIRE(csp.size() == 3);
	}

	SECTION("span<T, N> sp")
	{
		int arr[] = {1, 2, 3, 4, 5, 6};
		cl::span<int, sizeof(arr)/sizeof(*arr)> sp(arr);
		cl::content_span<int, sizeof(arr)/sizeof(*arr)> csp(sp);

		REQUIRE(csp.size() == sizeof(arr)/sizeof(*arr));
		REQUIRE(std::equal(std::cbegin(csp), std::cend(csp), std::cbegin(arr)));
	}

	SECTION("std::array<T> const& v")
	{
		std::array<int, 3> a = {1, 2, 3};
		cl::content_span<int, 3> csp(a);

		REQUIRE(csp.size() == a.size());
		REQUIRE(std::equal(std::cbegin(csp), std::cend(csp), std::cbegin(a)));
	}
}

TEST_CASE("algorithms")
{

	SECTION("creating content_span")
	{
		auto csp = cl::generate_n(10, []{ return cl::random_number(0, 100); });

		cl::for_each(csp, [](int i){
			std::cout << i << '\n';
		});
	}
}


