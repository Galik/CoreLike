//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include "test.hpp"

#include <iostream>

#include "../include/corelike/span_allocator.hpp"
#include "../include/corelike/span_algorithm.hpp"

namespace cl { using namespace galik::corelike; }

TEST_CASE("Framework")
{
	SECTION("creating")
	{
		cl::span_allocator<int> sa(100);

		cl::optional<cl::span<int>> osp;

		REQUIRE_NOTHROW(osp = sa.allocate(10));
		REQUIRE(osp);

		cl::generate(osp.value(), []{ return cl::random_number(0, 100); });
		REQUIRE_NOTHROW(osp = sa.reallocate(osp.value(), 15));

		auto ssp = cl::find_if(osp.value(), [](int i){ return i > 5; });

		REQUIRE_NOTHROW(sa.deallocate(ssp));
	}

	SECTION("stuff")
	{
		cl::span_allocator<int> sa(100);

		cl::optional<cl::span<int>> osp;

		REQUIRE_NOTHROW(osp = sa.allocate(10));
		REQUIRE(osp);

		cl::generate(osp.value(), []{ return cl::random_number(0, 100); });
		REQUIRE_NOTHROW(osp = sa.reallocate(osp.value(), 15));

		auto ssp = cl::find_if(osp.value(), [](int i){ return i > 5; });

		REQUIRE_NOTHROW(sa.deallocate(ssp));
	}
}


