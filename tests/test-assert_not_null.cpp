//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <vector>
#include "../include/corelike/corelike.hpp"

namespace cl { using namespace galik::corelike; }

void protected_function(cl::assert_not_null<int*>) {}

void protected_const_function(cl::assert_not_null<int const*>) {}

TEST_CASE("Call")
{
	SECTION("non-const")
	{
		int i = 2;
		int* np = nullptr;
//		int const* cnp = nullptr;
		int* ip = &i;
//		int const* cip = &i;

//		REQUIRE_THROWS(protected_function(nullptr));
		REQUIRE_THROWS(protected_function(np));
//		REQUIRE_THROWS(protected_function(cnp));

		REQUIRE_NOTHROW(protected_function(&i));

		REQUIRE_NOTHROW(protected_function(ip));
//		REQUIRE_NOTHROW(protected_function(cip));
	}

	SECTION("const")
	{
		int i = 2;
		int* np = nullptr;
		int const* cnp = nullptr;
		int* ip = &i;
		int const* cip = &i;

//		REQUIRE_THROWS(protected_const_function(nullptr));
		REQUIRE_THROWS(protected_const_function(np));
		REQUIRE_THROWS(protected_const_function(cnp));

		REQUIRE_NOTHROW(protected_const_function(&i));

		REQUIRE_NOTHROW(protected_const_function(ip));
		REQUIRE_NOTHROW(protected_const_function(cip));
	}
}


