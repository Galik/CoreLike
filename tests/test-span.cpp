//
// Copyright (c) 2018 Galik <galik.bool@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <vector>
#include "../include/corelike/span_algorithm.hpp"

namespace cl { using namespace galik::corelike; }

TEST_CASE("Framework")
{
	SECTION("creating split_span")
	{
		std::vector<int> v1;
		std::vector<int> v2 = {1, 2, 3};

		cl::span<int> sp0;
		cl::span<int> sp1(v1);
		cl::span<int> sp2(v2);

		auto ssp0 = sp0.split();
		auto ssp1 = sp1.split();
		auto ssp2 = sp2.split();

		REQUIRE(sp0.empty());
		REQUIRE(sp0.size() == 0);
		REQUIRE(sp0.data() == nullptr);

		REQUIRE(sp1.empty());
		REQUIRE(sp1.size() == 0);
		REQUIRE(sp1.data() == nullptr);

		REQUIRE(!sp2.empty());
		REQUIRE(sp2.size() == v2.size());
		REQUIRE(sp2.data() == v2.data());

		REQUIRE(ssp0.empty());
		REQUIRE(ssp0.size() == 0);
		REQUIRE(ssp0.data() == nullptr);

		REQUIRE(ssp1.empty());
		REQUIRE(ssp1.size() == 0);
		REQUIRE(ssp1.data() == nullptr);

		REQUIRE(!ssp2.empty());
		REQUIRE(ssp2.size() == sp2.size());
		REQUIRE(ssp2.data() == sp2.data());

		ssp2 = sp2.split_at(2);

		REQUIRE(ssp2.size() == sp2.size());
		REQUIRE(ssp2 == sp2);
		REQUIRE(ssp2.left() == sp2.subspan(0, 2));
		REQUIRE(ssp2.right() == sp2.subspan(2));
	}

	SECTION("creating const split_span")
	{
		std::vector<int> v = {1, 2, 3};

		cl::const_span<int> sp(v);

		auto ssp = sp.split_at(2);

		REQUIRE(ssp == cl::span<int>(v));
		REQUIRE(ssp.left() == cl::span<int>(v).subspan(0, 2));
	}
}

TEST_CASE("Construction")
{
	SECTION("default")
	{
		cl::span<int> sp{};

		REQUIRE(sp.empty());
		REQUIRE(sp.size() == 0);
		REQUIRE(sp.data() == nullptr);
	}

	SECTION("const, non const")
	{
		int a[] = {1};
		int const ca[] = {1};

		std::array<int, 1> sa = {1};
		std::array<int, 1> const csa = {1};

		std::vector<int> v = {1};
		std::vector<int> const cv = {1};

		cl::span<int> spa(a);
//		cl::span<int> spca(ca); // fails to compile (good)

		cl::span<int> spsa(sa);
//		cl::span<int> spcsa(csa); // fails to compile (good)

		cl::span<int> spv(v);
//		cl::span<int> spcv(cv); // fails to compile (good)

		cl::span<int> spvpn(v.data(), v.size());
//		cl::span<int> spcvpn(cv.data(), cv.size()); // fails to compile (good)

		cl::span<int> spvpp(v.data(), v.data() + v.size());
//		cl::span<int> spcvpp(cv.data(), cv.data() + cv.size()); // fails to compile (good)

		cl::const_span<int> cspa(a);
		cl::const_span<int> cspca(ca);

		cl::const_span<int> cspsa(sa);
		cl::const_span<int> cspcsa(csa);

		cl::const_span<int> cspv(v);
		cl::const_span<int> cspcv(cv);

		cl::const_span<int> cspvpn(v.data(), v.size());
		cl::const_span<int> cspcvpn(cv.data(), cv.size());

		cl::const_span<int> cspvpp(v.data(), v.data() + v.size());
		cl::const_span<int> cspcvpp(cv.data(), cv.data() + cv.size());
	}

	SECTION("pointers")
	{
		std::vector<int> v = {1, 2, 3};
		cl::span<int> sp{v.data(), v.data() + v.size()};

		REQUIRE(!sp.empty());
		REQUIRE(sp.size() == v.size());
	}

	SECTION("array[]")
	{
		int v[] = {1, 2, 3};
		cl::span<int> sp{v};

		REQUIRE(!sp.empty());
		REQUIRE(sp.size() == sizeof(v)/sizeof(v[0]));
	}

// ISO C++ forbids zero size array[]
//	SECTION("array[] (empty)")
//	{
//		int v[0];
//		cl::span<int> sp{v};
//
//		REQUIRE(sp.empty());
//		REQUIRE(sp.size() == 0);
//		REQUIRE(sp.data() == nullptr);
//		REQUIRE(sp.begin() == nullptr);
//		REQUIRE(sp.end() == nullptr);
//	}

	SECTION("std::array")
	{
		std::array<int, 3> v = {1, 2, 3};
		cl::span<int> sp{v};

		REQUIRE(!sp.empty());
		REQUIRE(sp.size() == v.size());
	}

	SECTION("std::array (empty)")
	{
		std::array<int, 0> v;
		cl::span<int> sp{v};

		REQUIRE(sp.empty());
		REQUIRE(sp.size() == v.size());
		REQUIRE(sp.data() == nullptr);
		REQUIRE(sp.begin() == nullptr);
		REQUIRE(sp.end() == nullptr);
	}

	SECTION("container")
	{
		std::vector<int> const v = {1, 2, 3};
		cl::span<int const> sp{v};
		cl::const_span<int> csp{v};

		REQUIRE(!sp.empty());
		REQUIRE(sp.size() == v.size());
	}

	SECTION("container (empty)")
	{
		std::vector<int> const v;
		cl::span<int const> sp{v};
		cl::const_span<int> csp{v};

		REQUIRE(sp.empty());
		REQUIRE(sp.size() == v.size());
		REQUIRE(sp.data() == nullptr);
		REQUIRE(sp.begin() == nullptr);
		REQUIRE(sp.end() == nullptr);
	}

	SECTION("arbitrary data")
	{
		std::vector<int> const v = {1, 2, 3, 4, 5, 6, 7, 8};
		cl::span<int const> sp{v.data() + 2, v.data() + 5};
		cl::const_span<int> csp{v.data() + 2, v.data() + 5};

		REQUIRE(!sp.empty());
		REQUIRE(sp.size() == 3);
	}

	SECTION("arbitrary data (empty)")
	{
		std::vector<int> const v = {1, 2, 3, 4, 5, 6, 7, 8};
		cl::span<int const> sp{v.data() + 2, v.data() + 2};
		cl::const_span<int> csp{v.data() + 2, v.data() + 2};

		REQUIRE(sp.empty());
		REQUIRE(sp.size() == 0);
		REQUIRE(sp.data() == v.data() + 2);
		REQUIRE(sp.begin() == v.data() + 2);
		REQUIRE(sp.end() == v.data() + 2);
	}
}

TEST_CASE("Comparators")
{
	std::string sa = "123";
	std::string sb = "123";
	std::string sc = "124";

	cl::span<char> a(&sa[0], sa.size());
	cl::span<char> b(&sb[0], sb.size());
	cl::const_span<char> c(&sc[0], sc.size());

	SECTION("<")
	{
		REQUIRE_FALSE(a < a);
		REQUIRE_FALSE(a < b);
		REQUIRE      (a < c);

		REQUIRE_FALSE(b < a);
		REQUIRE_FALSE(b < b);
		REQUIRE      (b < c);

		REQUIRE_FALSE(c < a);
		REQUIRE_FALSE(c < b);
		REQUIRE_FALSE(c < c);
	}

	SECTION(">")
	{
		REQUIRE_FALSE(a > a);
		REQUIRE_FALSE(a > b);
		REQUIRE_FALSE(a > c);

		REQUIRE_FALSE(b > a);
		REQUIRE_FALSE(b > b);
		REQUIRE_FALSE(b > c);

		REQUIRE      (c > a);
		REQUIRE      (c > b);
		REQUIRE_FALSE(c > c);
	}

	SECTION("==")
	{
		REQUIRE      (a == a);
		REQUIRE      (a == b);
		REQUIRE_FALSE(a == c);

		REQUIRE      (b == a);
		REQUIRE      (b == b);
		REQUIRE_FALSE(b == c);

		REQUIRE_FALSE(c == a);
		REQUIRE_FALSE(c == b);
		REQUIRE      (c == c);
	}

	SECTION("!=")
	{
		REQUIRE_FALSE(a != a);
		REQUIRE_FALSE(a != b);
		REQUIRE      (a != c);

		REQUIRE_FALSE(b != a);
		REQUIRE_FALSE(b != b);
		REQUIRE      (b != c);

		REQUIRE      (c != a);
		REQUIRE      (c != b);
		REQUIRE_FALSE(c != c);
	}

	SECTION("<=")
	{
		REQUIRE      (a <= a);
		REQUIRE      (a <= b);
		REQUIRE      (a <= c);

		REQUIRE      (b <= a);
		REQUIRE      (b <= b);
		REQUIRE      (b <= c);

		REQUIRE_FALSE(c <= a);
		REQUIRE_FALSE(c <= b);
		REQUIRE      (c <= c);
	}

	SECTION(">=")
	{
		REQUIRE      (a >= a);
		REQUIRE      (a >= b);
		REQUIRE_FALSE(a >= c);

		REQUIRE      (b >= a);
		REQUIRE      (b >= b);
		REQUIRE_FALSE(b >= c);

		REQUIRE      (c >= a);
		REQUIRE      (c >= b);
		REQUIRE      (c >= c);
	}
}

template<typename T, std::size_t N>
constexpr std::size_t array_size(T(&)[N]) { return N; }

TEST_CASE("Fixed size spans")
{
	SECTION("Construction")
	{
		int arr[] {0, 1, 2};
		cl::span<int, array_size(arr)> sp1(arr);
		cl::span<int, array_size(arr)> sp2(arr);
		cl::span<int, array_size(arr) - 1> sp3(arr);

		REQUIRE(sp1.size() == array_size(arr));
		REQUIRE(sp1 == sp2);

		REQUIRE(sp3.size() != array_size(arr));
		REQUIRE(sp3 != sp2);
	}
}

TEST_CASE("Algorithms")
{
	SECTION("all_of")
	{
		std::vector<int> v = {1, 2, 3};
		cl::span<int> sp{v};

		REQUIRE(cl::all_of(sp, [](int i){ return i > 0; }));
	}

	SECTION("any_of")
	{
		std::vector<int> v = {1, 2, 3};
		cl::span<int> sp{v};

		REQUIRE(cl::any_of(sp, [](int i){ return i < 2; }));
	}

	SECTION("none_of")
	{
		std::vector<int> v = {1, 2, 3};
		cl::span<int> sp{v};

		REQUIRE(cl::none_of(sp, [](int i){ return i < 0; }));
	}

	SECTION("for_each")
	{
		std::vector<int> v = {1, 2, 3};
		cl::span<int> sp{v};

		cl::for_each(sp, [](int& i){ i += 1; });

		REQUIRE(v == std::vector<int>{2, 3, 4});
	}

	SECTION("count")
	{
		std::vector<int> v = {1, 2, 3, 2};
		cl::span<int> sp{v};

		REQUIRE(cl::count(sp, 2) == 2);
	}

	SECTION("count_if")
	{
		std::vector<int> v = {1, 2, 3, 4};
		cl::span<int> sp{v};
		cl::const_span<int> csp(v);

		REQUIRE(cl::count_if(sp, [](int i){ return i < 3; }) == 2);
		REQUIRE(cl::count_if(csp, [](int i){ return i < 3; }) == 2);
	}

	SECTION("mismatch")
	{
		std::vector<int> v1 = {1, 2, 3, 4};
		std::vector<int> v2 = {1, 2, 5, 6};

		cl::span<int> sp1{v1};
		cl::span<int> sp2{v2};

		auto mm = cl::mismatch(sp1, sp2);

		REQUIRE(mm.first == sp1);
		REQUIRE(mm.second == sp2);

		REQUIRE(mm.first.left().size() == 2);
		REQUIRE(mm.first.right().size() == 2);
		REQUIRE(mm.second.left().size() == 2);
		REQUIRE(mm.second.right().size() == 2);

		REQUIRE(*mm.first.mid() == 3);
		REQUIRE(*mm.second.mid() == 5);
	}

	SECTION("find")
	{
		std::vector<int> v = {1, 2, 3, 4};

		cl::span<int> sp{v};

		auto found = cl::find(sp, 3);

		REQUIRE(found.left().size() == 2);
		REQUIRE(found.right().size() == 2);
	}

	SECTION("find_if")
	{
		std::vector<int> v = {1, 2, 3, 4};

		cl::span<int> sp{v};

		auto found = cl::find_if(sp, [](int i){ return i > 3; });

		REQUIRE(found.left().size() == 3);
		REQUIRE(found.right().size() == 1);
	}

	SECTION("find_if_not")
	{
		std::vector<int> v = {1, 2, 3, 4};

		cl::span<int> sp{v};

		auto found = cl::find_if_not(sp, [](int i){ return i > 3; });

		REQUIRE(found.left().size() == 0);
		REQUIRE(found.right().size() == 4);
	}
}


